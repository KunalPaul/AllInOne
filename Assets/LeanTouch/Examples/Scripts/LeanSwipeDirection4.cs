using UnityEngine;
using UnityEngine.UI;

namespace Lean.Touch
{
	// This script will tell you which direction you swiped in
	public class LeanSwipeDirection4 : MonoBehaviour
	{
		[Tooltip("The text element we will display the swipe information in")]
		public Text InfoText;
		public Animator anim;
	
		protected virtual void OnEnable()
		{
			// Hook into the events we need
			LeanTouch.OnFingerSwipe += OnFingerSwipe;
		}
	
		protected virtual void OnDisable()
		{
			// Unhook the events
			LeanTouch.OnFingerSwipe -= OnFingerSwipe;
		}

		void Update(){

			if (Input.GetKeyDown (KeyCode.A)) {
				anim.Play ("Scrollbar move");
			}

			if (Input.GetKeyDown (KeyCode.S)) {
				anim.Play ("Scrollbar move 0");
			}

		}
	
		public void OnFingerSwipe(LeanFinger finger)
		{
			// Make sure the info text exists
			if (InfoText != null)
			{
				// Store the swipe delta in a temp variable
				var swipe = finger.SwipeScreenDelta;
			
				if (swipe.x < -Mathf.Abs(swipe.y))
				{
					InfoText.text = "You swiped left!";
					anim.Play ("Scrollbar move 0");
					Debug.Log ("i m false");
				}
			
				if (swipe.x > Mathf.Abs(swipe.y))
				{
					InfoText.text = "You swiped right!";
					anim.Play ("Scrollbar move");
					Debug.Log ("i m true");
				}
			
				if (swipe.y < -Mathf.Abs(swipe.x))
				{
					InfoText.text = "You swiped down!";
				}
			
				if (swipe.y > Mathf.Abs(swipe.x))
				{
					InfoText.text = "You swiped up!";
				}
			}
		}
	}
}