﻿using UnityEngine;
using System.Collections;

public class AnimatorSpeed : MonoBehaviour {

	public Animator anim;

	void Update () {
	
	}

	public void IncreasedSpeed30(){

		anim.speed = 2f;
	}
	public void IncreasedSpeed60(){

		anim.speed = 3f;
	}
	public void IncreasedSpeed90(){

		anim.speed = 6f;
	}
}
