﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour {

	AsyncOperation ao;
	public Image LoadingImage;
	public Text LoadingText;

	void Start () {

		ao = Application.LoadLevelAsync (1);
	}

	void Update () {
		if (ao != null) {

			LoadingImage.fillAmount = ao.progress;
	
			LoadingText.text="LOADING"+" "+Mathf.Round(ao.progress*100)+" "+"%";

		}
	
	}
		
}
