﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NextsScence : MonoBehaviour {

    AsyncOperation ao;
    public Image LoadingImage;
    public Text LoadingText;
    public GameObject loading;

    void Start(){
        loading.SetActive(false);
    }

    public void Move(){
        loading.SetActive(true);
		ao = Application.LoadLevelAsync(Application.loadedLevel+1);
	}

    void Update()
    {
        if (ao != null)
        {

            LoadingImage.fillAmount = ao.progress;

            LoadingText.text = "LOADING" + " " + Mathf.Round(ao.progress * 100) + " " + "%";

        }

    }
}
