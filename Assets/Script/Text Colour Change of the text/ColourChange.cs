﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColourChange : MonoBehaviour {

	public Text text1;
	public Text text2;
	public Text text3;

	void Start () {
	
	}
	

	void Update () {

		if (Application.loadedLevel==1) {
			text1.color = Color.yellow;
			text2.color = Color.white;
			text3.color = Color.white;
		}
		if (Application.loadedLevel==2) {
			text1.color = Color.white;
			text2.color = Color.yellow;
			text3.color = Color.white;
		}

		if (Application.loadedLevel==3) {
			text1.color = Color.white;
			text2.color = Color.white;
			text3.color = Color.yellow;
		}

	
	}
}
