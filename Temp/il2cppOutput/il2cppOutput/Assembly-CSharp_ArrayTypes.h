﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Lean.Touch.LeanDragTrail/Link
struct Link_t390936538;
// Lean.Touch.LeanSnapshot
struct LeanSnapshot_t1117222592;
// Lean.Touch.LeanFingerHeld
struct LeanFingerHeld_t681037536;
// Lean.Touch.LeanFingerHeld/Link
struct Link_t182636800;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;
// Lean.Touch.LeanSelectable
struct LeanSelectable_t3692576450;
// Lean.Touch.LeanTouch
struct LeanTouch_t1022081457;
// VideoPlaybackBehaviour
struct VideoPlaybackBehaviour_t222960481;
// Vuforia.WireframeBehaviour
struct WireframeBehaviour_t2494532455;

#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanDragTrail_Link390936538.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSnapshot1117222592.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld681037536.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld_Link182636800.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectable3692576450.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTouch1022081457.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour222960481.h"
#include "AssemblyU2DCSharp_Vuforia_WireframeBehaviour2494532455.h"

#pragma once
// Lean.Touch.LeanDragTrail/Link[]
struct LinkU5BU5D_t793426815  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t390936538 * m_Items[1];

public:
	inline Link_t390936538 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t390936538 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t390936538 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Link_t390936538 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t390936538 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t390936538 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Lean.Touch.LeanSnapshot[]
struct LeanSnapshotU5BU5D_t3359118913  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LeanSnapshot_t1117222592 * m_Items[1];

public:
	inline LeanSnapshot_t1117222592 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LeanSnapshot_t1117222592 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LeanSnapshot_t1117222592 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LeanSnapshot_t1117222592 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LeanSnapshot_t1117222592 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LeanSnapshot_t1117222592 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Lean.Touch.LeanFingerHeld[]
struct LeanFingerHeldU5BU5D_t1367849377  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LeanFingerHeld_t681037536 * m_Items[1];

public:
	inline LeanFingerHeld_t681037536 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LeanFingerHeld_t681037536 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LeanFingerHeld_t681037536 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LeanFingerHeld_t681037536 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LeanFingerHeld_t681037536 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LeanFingerHeld_t681037536 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Lean.Touch.LeanFingerHeld/Link[]
struct LinkU5BU5D_t4064987905  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Link_t182636800 * m_Items[1];

public:
	inline Link_t182636800 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Link_t182636800 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Link_t182636800 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Link_t182636800 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Link_t182636800 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Link_t182636800 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Lean.Touch.LeanFinger[]
struct LeanFingerU5BU5D_t2606873688  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LeanFinger_t76062517 * m_Items[1];

public:
	inline LeanFinger_t76062517 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LeanFinger_t76062517 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LeanFinger_t76062517 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LeanFinger_t76062517 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LeanFinger_t76062517 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LeanFinger_t76062517 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Lean.Touch.LeanSelectable[]
struct LeanSelectableU5BU5D_t568882167  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LeanSelectable_t3692576450 * m_Items[1];

public:
	inline LeanSelectable_t3692576450 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LeanSelectable_t3692576450 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LeanSelectable_t3692576450 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LeanSelectable_t3692576450 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LeanSelectable_t3692576450 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LeanSelectable_t3692576450 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Lean.Touch.LeanTouch[]
struct LeanTouchU5BU5D_t4228111468  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) LeanTouch_t1022081457 * m_Items[1];

public:
	inline LeanTouch_t1022081457 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline LeanTouch_t1022081457 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, LeanTouch_t1022081457 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline LeanTouch_t1022081457 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline LeanTouch_t1022081457 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, LeanTouch_t1022081457 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// VideoPlaybackBehaviour[]
struct VideoPlaybackBehaviourU5BU5D_t2624769532  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) VideoPlaybackBehaviour_t222960481 * m_Items[1];

public:
	inline VideoPlaybackBehaviour_t222960481 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VideoPlaybackBehaviour_t222960481 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VideoPlaybackBehaviour_t222960481 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline VideoPlaybackBehaviour_t222960481 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VideoPlaybackBehaviour_t222960481 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VideoPlaybackBehaviour_t222960481 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Vuforia.WireframeBehaviour[]
struct WireframeBehaviourU5BU5D_t2935582494  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) WireframeBehaviour_t2494532455 * m_Items[1];

public:
	inline WireframeBehaviour_t2494532455 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline WireframeBehaviour_t2494532455 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, WireframeBehaviour_t2494532455 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline WireframeBehaviour_t2494532455 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline WireframeBehaviour_t2494532455 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, WireframeBehaviour_t2494532455 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
