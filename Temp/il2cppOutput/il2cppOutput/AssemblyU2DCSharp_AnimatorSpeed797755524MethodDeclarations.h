﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatorSpeed
struct AnimatorSpeed_t797755524;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimatorSpeed::.ctor()
extern "C"  void AnimatorSpeed__ctor_m1202624467 (AnimatorSpeed_t797755524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorSpeed::Update()
extern "C"  void AnimatorSpeed_Update_m626783974 (AnimatorSpeed_t797755524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorSpeed::IncreasedSpeed30()
extern "C"  void AnimatorSpeed_IncreasedSpeed30_m4060863799 (AnimatorSpeed_t797755524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorSpeed::IncreasedSpeed60()
extern "C"  void AnimatorSpeed_IncreasedSpeed60_m505208414 (AnimatorSpeed_t797755524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatorSpeed::IncreasedSpeed90()
extern "C"  void AnimatorSpeed_IncreasedSpeed90_m1325332229 (AnimatorSpeed_t797755524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
