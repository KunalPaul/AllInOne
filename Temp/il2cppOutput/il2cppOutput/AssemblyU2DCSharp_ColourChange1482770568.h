﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ColourChange
struct  ColourChange_t1482770568  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text ColourChange::text1
	Text_t356221433 * ___text1_2;
	// UnityEngine.UI.Text ColourChange::text2
	Text_t356221433 * ___text2_3;
	// UnityEngine.UI.Text ColourChange::text3
	Text_t356221433 * ___text3_4;

public:
	inline static int32_t get_offset_of_text1_2() { return static_cast<int32_t>(offsetof(ColourChange_t1482770568, ___text1_2)); }
	inline Text_t356221433 * get_text1_2() const { return ___text1_2; }
	inline Text_t356221433 ** get_address_of_text1_2() { return &___text1_2; }
	inline void set_text1_2(Text_t356221433 * value)
	{
		___text1_2 = value;
		Il2CppCodeGenWriteBarrier(&___text1_2, value);
	}

	inline static int32_t get_offset_of_text2_3() { return static_cast<int32_t>(offsetof(ColourChange_t1482770568, ___text2_3)); }
	inline Text_t356221433 * get_text2_3() const { return ___text2_3; }
	inline Text_t356221433 ** get_address_of_text2_3() { return &___text2_3; }
	inline void set_text2_3(Text_t356221433 * value)
	{
		___text2_3 = value;
		Il2CppCodeGenWriteBarrier(&___text2_3, value);
	}

	inline static int32_t get_offset_of_text3_4() { return static_cast<int32_t>(offsetof(ColourChange_t1482770568, ___text3_4)); }
	inline Text_t356221433 * get_text3_4() const { return ___text3_4; }
	inline Text_t356221433 ** get_address_of_text3_4() { return &___text3_4; }
	inline void set_text3_4(Text_t356221433 * value)
	{
		___text3_4 = value;
		Il2CppCodeGenWriteBarrier(&___text3_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
