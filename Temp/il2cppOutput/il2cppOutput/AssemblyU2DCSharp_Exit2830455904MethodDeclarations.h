﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Exit
struct Exit_t2830455904;

#include "codegen/il2cpp-codegen.h"

// System.Void Exit::.ctor()
extern "C"  void Exit__ctor_m3548972811 (Exit_t2830455904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Exit::Update()
extern "C"  void Exit_Update_m3047017570 (Exit_t2830455904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
