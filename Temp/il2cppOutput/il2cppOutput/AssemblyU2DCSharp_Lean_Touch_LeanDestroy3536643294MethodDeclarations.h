﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanDestroy
struct LeanDestroy_t3536643294;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanDestroy::.ctor()
extern "C"  void LeanDestroy__ctor_m397556468 (LeanDestroy_t3536643294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDestroy::Update()
extern "C"  void LeanDestroy_Update_m837232565 (LeanDestroy_t3536643294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDestroy::DestroyNow()
extern "C"  void LeanDestroy_DestroyNow_m3404985960 (LeanDestroy_t3536643294 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
