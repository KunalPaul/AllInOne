﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanDragTrail
struct LeanDragTrail_t3533323662;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;
// Lean.Touch.LeanDragTrail/Link
struct Link_t390936538;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanDragTrail::.ctor()
extern "C"  void LeanDragTrail__ctor_m435463780 (LeanDragTrail_t3533323662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDragTrail::OnEnable()
extern "C"  void LeanDragTrail_OnEnable_m764732548 (LeanDragTrail_t3533323662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDragTrail::OnDisable()
extern "C"  void LeanDragTrail_OnDisable_m1079759985 (LeanDragTrail_t3533323662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDragTrail::WritePositions(UnityEngine.LineRenderer,Lean.Touch.LeanFinger)
extern "C"  void LeanDragTrail_WritePositions_m485574347 (LeanDragTrail_t3533323662 * __this, LineRenderer_t849157671 * ___line0, LeanFinger_t76062517 * ___finger1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDragTrail::OnFingerSet(Lean.Touch.LeanFinger)
extern "C"  void LeanDragTrail_OnFingerSet_m2826470018 (LeanDragTrail_t3533323662 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDragTrail::OnFingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanDragTrail_OnFingerUp_m2264237025 (LeanDragTrail_t3533323662 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Lean.Touch.LeanDragTrail/Link Lean.Touch.LeanDragTrail::FindLink(Lean.Touch.LeanFinger)
extern "C"  Link_t390936538 * LeanDragTrail_FindLink_m2824318544 (LeanDragTrail_t3533323662 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
