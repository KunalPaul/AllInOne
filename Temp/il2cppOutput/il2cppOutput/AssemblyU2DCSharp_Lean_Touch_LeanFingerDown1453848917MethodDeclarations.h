﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerDown
struct LeanFingerDown_t1453848917;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanFingerDown::.ctor()
extern "C"  void LeanFingerDown__ctor_m3033129109 (LeanFingerDown_t1453848917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerDown::OnEnable()
extern "C"  void LeanFingerDown_OnEnable_m525475781 (LeanFingerDown_t1453848917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerDown::OnDisable()
extern "C"  void LeanFingerDown_OnDisable_m2363167140 (LeanFingerDown_t1453848917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerDown::FingerDown(Lean.Touch.LeanFinger)
extern "C"  void LeanFingerDown_FingerDown_m549979536 (LeanFingerDown_t1453848917 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
