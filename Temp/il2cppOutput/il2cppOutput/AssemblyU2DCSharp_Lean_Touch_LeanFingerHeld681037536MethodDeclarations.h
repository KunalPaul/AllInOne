﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerHeld
struct LeanFingerHeld_t681037536;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;
// Lean.Touch.LeanFingerHeld/Link
struct Link_t182636800;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanFingerHeld::.ctor()
extern "C"  void LeanFingerHeld__ctor_m2699747998 (LeanFingerHeld_t681037536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerHeld::OnEnable()
extern "C"  void LeanFingerHeld_OnEnable_m4211930482 (LeanFingerHeld_t681037536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerHeld::OnDisable()
extern "C"  void LeanFingerHeld_OnDisable_m3553308763 (LeanFingerHeld_t681037536 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerHeld::OnFingerDown(Lean.Touch.LeanFinger)
extern "C"  void LeanFingerHeld_OnFingerDown_m315889834 (LeanFingerHeld_t681037536 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerHeld::OnFingerSet(Lean.Touch.LeanFinger)
extern "C"  void LeanFingerHeld_OnFingerSet_m248884212 (LeanFingerHeld_t681037536 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerHeld::OnFingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanFingerHeld_OnFingerUp_m404005487 (LeanFingerHeld_t681037536 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Lean.Touch.LeanFingerHeld/Link Lean.Touch.LeanFingerHeld::FindLink(Lean.Touch.LeanFinger)
extern "C"  Link_t182636800 * LeanFingerHeld_FindLink_m3737920624 (LeanFingerHeld_t681037536 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerHeld::.cctor()
extern "C"  void LeanFingerHeld__cctor_m1813857033 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
