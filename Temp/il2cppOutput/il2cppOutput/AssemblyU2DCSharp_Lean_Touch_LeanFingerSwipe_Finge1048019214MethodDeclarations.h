﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerSwipe/FingerEvent
struct FingerEvent_t1048019214;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanFingerSwipe/FingerEvent::.ctor()
extern "C"  void FingerEvent__ctor_m1168813501 (FingerEvent_t1048019214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
