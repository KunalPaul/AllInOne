﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerTap
struct LeanFingerTap_t2593896000;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanFingerTap::.ctor()
extern "C"  void LeanFingerTap__ctor_m1218480372 (LeanFingerTap_t2593896000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerTap::OnEnable()
extern "C"  void LeanFingerTap_OnEnable_m960515320 (LeanFingerTap_t2593896000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerTap::OnDisable()
extern "C"  void LeanFingerTap_OnDisable_m2849829899 (LeanFingerTap_t2593896000 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerTap::FingerTap(Lean.Touch.LeanFinger)
extern "C"  void LeanFingerTap_FingerTap_m2950933368 (LeanFingerTap_t2593896000 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
