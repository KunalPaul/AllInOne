﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanMultiTap
struct LeanMultiTap_t2693737624;
// System.Collections.Generic.List`1<Lean.Touch.LeanFinger>
struct List_1_t3740150945;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanMultiTap::.ctor()
extern "C"  void LeanMultiTap__ctor_m1701618822 (LeanMultiTap_t2693737624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanMultiTap::Update()
extern "C"  void LeanMultiTap_Update_m2147604867 (LeanMultiTap_t2693737624 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Lean.Touch.LeanMultiTap::GetFingerCount(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern "C"  int32_t LeanMultiTap_GetFingerCount_m2420238986 (LeanMultiTap_t2693737624 * __this, List_1_t3740150945 * ___fingers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
