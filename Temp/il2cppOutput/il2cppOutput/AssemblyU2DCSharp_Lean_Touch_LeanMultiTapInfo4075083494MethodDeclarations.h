﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanMultiTapInfo
struct LeanMultiTapInfo_t4075083494;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanMultiTapInfo::.ctor()
extern "C"  void LeanMultiTapInfo__ctor_m1409484742 (LeanMultiTapInfo_t4075083494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanMultiTapInfo::OnMultiTap(System.Int32,System.Int32)
extern "C"  void LeanMultiTapInfo_OnMultiTap_m3667847953 (LeanMultiTapInfo_t4075083494 * __this, int32_t ___count0, int32_t ___highest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
