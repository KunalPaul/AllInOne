﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanMultiTap/IntEvent
struct IntEvent_t18007207;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanMultiTap/IntEvent::.ctor()
extern "C"  void IntEvent__ctor_m3595511254 (IntEvent_t18007207 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
