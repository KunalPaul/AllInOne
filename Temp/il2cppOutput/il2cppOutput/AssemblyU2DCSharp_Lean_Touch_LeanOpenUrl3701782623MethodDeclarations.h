﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanOpenUrl
struct LeanOpenUrl_t3701782623;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String2029220233.h"

// System.Void Lean.Touch.LeanOpenUrl::.ctor()
extern "C"  void LeanOpenUrl__ctor_m2840547233 (LeanOpenUrl_t3701782623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanOpenUrl::Open(System.String)
extern "C"  void LeanOpenUrl_Open_m1354562859 (LeanOpenUrl_t3701782623 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
