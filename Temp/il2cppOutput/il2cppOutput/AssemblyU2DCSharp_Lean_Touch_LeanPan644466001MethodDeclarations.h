﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanPan
struct LeanPan_t644466001;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanPan::.ctor()
extern "C"  void LeanPan__ctor_m1307377581 (LeanPan_t644466001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPan::LateUpdate()
extern "C"  void LeanPan_LateUpdate_m2217208506 (LeanPan_t644466001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
