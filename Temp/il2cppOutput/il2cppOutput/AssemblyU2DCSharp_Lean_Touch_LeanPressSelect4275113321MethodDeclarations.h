﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanPressSelect
struct LeanPressSelect_t4275113321;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;
// UnityEngine.Component
struct Component_t3819376471;
// Lean.Touch.LeanSelectable
struct LeanSelectable_t3692576450;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectable3692576450.h"

// System.Void Lean.Touch.LeanPressSelect::.ctor()
extern "C"  void LeanPressSelect__ctor_m2730820493 (LeanPressSelect_t4275113321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::OnEnable()
extern "C"  void LeanPressSelect_OnEnable_m391324981 (LeanPressSelect_t4275113321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::OnDisable()
extern "C"  void LeanPressSelect_OnDisable_m1992781752 (LeanPressSelect_t4275113321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::FingerDown(Lean.Touch.LeanFinger)
extern "C"  void LeanPressSelect_FingerDown_m3715657148 (LeanPressSelect_t4275113321 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::FingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanPressSelect_FingerUp_m1384580525 (LeanPressSelect_t4275113321 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::Select(Lean.Touch.LeanFinger)
extern "C"  void LeanPressSelect_Select_m407632401 (LeanPressSelect_t4275113321 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::Select(Lean.Touch.LeanFinger,UnityEngine.Component)
extern "C"  void LeanPressSelect_Select_m220284365 (LeanPressSelect_t4275113321 * __this, LeanFinger_t76062517 * ___finger0, Component_t3819376471 * ___component1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::Select(Lean.Touch.LeanFinger,Lean.Touch.LeanSelectable)
extern "C"  void LeanPressSelect_Select_m1042001244 (LeanPressSelect_t4275113321 * __this, LeanFinger_t76062517 * ___finger0, LeanSelectable_t3692576450 * ___selectable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::DeselectAll()
extern "C"  void LeanPressSelect_DeselectAll_m1698992741 (LeanPressSelect_t4275113321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::Deselect(Lean.Touch.LeanSelectable)
extern "C"  void LeanPressSelect_Deselect_m668648615 (LeanPressSelect_t4275113321 * __this, LeanSelectable_t3692576450 * ___selectable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPressSelect::DeselectAndRemove(Lean.Touch.LeanSelectable)
extern "C"  void LeanPressSelect_DeselectAndRemove_m1686082056 (LeanPressSelect_t4275113321 * __this, LeanSelectable_t3692576450 * ___selectable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
