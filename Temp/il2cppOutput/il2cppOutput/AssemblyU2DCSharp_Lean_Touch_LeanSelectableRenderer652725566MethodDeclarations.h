﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSelectableRendererColor
struct LeanSelectableRendererColor_t652725566;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Lean.Touch.LeanSelectableRendererColor::.ctor()
extern "C"  void LeanSelectableRendererColor__ctor_m1392686636 (LeanSelectableRendererColor_t652725566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableRendererColor::Awake()
extern "C"  void LeanSelectableRendererColor_Awake_m3282447817 (LeanSelectableRendererColor_t652725566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectableRendererColor_OnSelect_m3213397893 (LeanSelectableRendererColor_t652725566 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableRendererColor::OnDeselect()
extern "C"  void LeanSelectableRendererColor_OnDeselect_m1681164812 (LeanSelectableRendererColor_t652725566 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableRendererColor::ChangeColor(UnityEngine.Color)
extern "C"  void LeanSelectableRendererColor_ChangeColor_m3455560813 (LeanSelectableRendererColor_t652725566 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
