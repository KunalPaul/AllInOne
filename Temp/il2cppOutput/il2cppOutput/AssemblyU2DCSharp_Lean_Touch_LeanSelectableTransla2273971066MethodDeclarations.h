﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSelectableTranslateInertia2D
struct LeanSelectableTranslateInertia2D_t2273971066;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSelectableTranslateInertia2D::.ctor()
extern "C"  void LeanSelectableTranslateInertia2D__ctor_m2316904878 (LeanSelectableTranslateInertia2D_t2273971066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableTranslateInertia2D::Update()
extern "C"  void LeanSelectableTranslateInertia2D_Update_m2839162865 (LeanSelectableTranslateInertia2D_t2273971066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableTranslateInertia2D::OnSelectUp(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectableTranslateInertia2D_OnSelectUp_m470194154 (LeanSelectableTranslateInertia2D_t2273971066 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
