﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSelectable/LeanFingerEvent
struct LeanFingerEvent_t2546124889;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanSelectable/LeanFingerEvent::.ctor()
extern "C"  void LeanFingerEvent__ctor_m1602871024 (LeanFingerEvent_t2546124889 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
