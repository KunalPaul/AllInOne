﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Animator
struct Animator_t69676727;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeDirection4
struct  LeanSwipeDirection4_t3774896743  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Lean.Touch.LeanSwipeDirection4::InfoText
	Text_t356221433 * ___InfoText_2;
	// UnityEngine.Animator Lean.Touch.LeanSwipeDirection4::anim
	Animator_t69676727 * ___anim_3;

public:
	inline static int32_t get_offset_of_InfoText_2() { return static_cast<int32_t>(offsetof(LeanSwipeDirection4_t3774896743, ___InfoText_2)); }
	inline Text_t356221433 * get_InfoText_2() const { return ___InfoText_2; }
	inline Text_t356221433 ** get_address_of_InfoText_2() { return &___InfoText_2; }
	inline void set_InfoText_2(Text_t356221433 * value)
	{
		___InfoText_2 = value;
		Il2CppCodeGenWriteBarrier(&___InfoText_2, value);
	}

	inline static int32_t get_offset_of_anim_3() { return static_cast<int32_t>(offsetof(LeanSwipeDirection4_t3774896743, ___anim_3)); }
	inline Animator_t69676727 * get_anim_3() const { return ___anim_3; }
	inline Animator_t69676727 ** get_address_of_anim_3() { return &___anim_3; }
	inline void set_anim_3(Animator_t69676727 * value)
	{
		___anim_3 = value;
		Il2CppCodeGenWriteBarrier(&___anim_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
