﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSwipeDirection4
struct LeanSwipeDirection4_t3774896743;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSwipeDirection4::.ctor()
extern "C"  void LeanSwipeDirection4__ctor_m99510987 (LeanSwipeDirection4_t3774896743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection4::OnEnable()
extern "C"  void LeanSwipeDirection4_OnEnable_m85345439 (LeanSwipeDirection4_t3774896743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection4::OnDisable()
extern "C"  void LeanSwipeDirection4_OnDisable_m636497210 (LeanSwipeDirection4_t3774896743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection4::Update()
extern "C"  void LeanSwipeDirection4_Update_m1547970012 (LeanSwipeDirection4_t3774896743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection4::OnFingerSwipe(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeDirection4_OnFingerSwipe_m677925979 (LeanSwipeDirection4_t3774896743 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
