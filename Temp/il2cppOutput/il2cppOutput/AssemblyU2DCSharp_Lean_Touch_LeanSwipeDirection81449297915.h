﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lean.Touch.LeanSwipeDirection8
struct  LeanSwipeDirection8_t1449297915  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.UI.Text Lean.Touch.LeanSwipeDirection8::InfoText
	Text_t356221433 * ___InfoText_2;
	// UnityEngine.GameObject Lean.Touch.LeanSwipeDirection8::Video
	GameObject_t1756533147 * ___Video_3;

public:
	inline static int32_t get_offset_of_InfoText_2() { return static_cast<int32_t>(offsetof(LeanSwipeDirection8_t1449297915, ___InfoText_2)); }
	inline Text_t356221433 * get_InfoText_2() const { return ___InfoText_2; }
	inline Text_t356221433 ** get_address_of_InfoText_2() { return &___InfoText_2; }
	inline void set_InfoText_2(Text_t356221433 * value)
	{
		___InfoText_2 = value;
		Il2CppCodeGenWriteBarrier(&___InfoText_2, value);
	}

	inline static int32_t get_offset_of_Video_3() { return static_cast<int32_t>(offsetof(LeanSwipeDirection8_t1449297915, ___Video_3)); }
	inline GameObject_t1756533147 * get_Video_3() const { return ___Video_3; }
	inline GameObject_t1756533147 ** get_address_of_Video_3() { return &___Video_3; }
	inline void set_Video_3(GameObject_t1756533147 * value)
	{
		___Video_3 = value;
		Il2CppCodeGenWriteBarrier(&___Video_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
