﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSwipeRigidbody2DNoRelease
struct LeanSwipeRigidbody2DNoRelease_t2933744255;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSwipeRigidbody2DNoRelease::.ctor()
extern "C"  void LeanSwipeRigidbody2DNoRelease__ctor_m3943121097 (LeanSwipeRigidbody2DNoRelease_t2933744255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2DNoRelease::OnEnable()
extern "C"  void LeanSwipeRigidbody2DNoRelease_OnEnable_m597269449 (LeanSwipeRigidbody2DNoRelease_t2933744255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2DNoRelease::OnDisable()
extern "C"  void LeanSwipeRigidbody2DNoRelease_OnDisable_m1079709910 (LeanSwipeRigidbody2DNoRelease_t2933744255 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2DNoRelease::OnFingerSet(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody2DNoRelease_OnFingerSet_m2317805067 (LeanSwipeRigidbody2DNoRelease_t2933744255 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2DNoRelease::OnFingerDown(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody2DNoRelease_OnFingerDown_m2503682295 (LeanSwipeRigidbody2DNoRelease_t2933744255 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2DNoRelease::OnFingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody2DNoRelease_OnFingerUp_m848186254 (LeanSwipeRigidbody2DNoRelease_t2933744255 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
