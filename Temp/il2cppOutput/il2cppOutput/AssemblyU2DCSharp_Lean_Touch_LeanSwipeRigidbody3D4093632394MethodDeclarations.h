﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSwipeRigidbody3D
struct LeanSwipeRigidbody3D_t4093632394;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSwipeRigidbody3D::.ctor()
extern "C"  void LeanSwipeRigidbody3D__ctor_m3917747106 (LeanSwipeRigidbody3D_t4093632394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3D::OnEnable()
extern "C"  void LeanSwipeRigidbody3D_OnEnable_m185868538 (LeanSwipeRigidbody3D_t4093632394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3D::OnDisable()
extern "C"  void LeanSwipeRigidbody3D_OnDisable_m1643139369 (LeanSwipeRigidbody3D_t4093632394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3D::OnFingerSwipe(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody3D_OnFingerSwipe_m3040858390 (LeanSwipeRigidbody3D_t4093632394 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
