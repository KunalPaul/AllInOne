﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanTranslate
struct LeanTranslate_t3292440434;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Lean.Touch.LeanTranslate::.ctor()
extern "C"  void LeanTranslate__ctor_m824126628 (LeanTranslate_t3292440434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTranslate::Update()
extern "C"  void LeanTranslate_Update_m1593674085 (LeanTranslate_t3292440434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTranslate::Translate(UnityEngine.Vector2)
extern "C"  void LeanTranslate_Translate_m410589770 (LeanTranslate_t3292440434 * __this, Vector2_t2243707579  ___screenDelta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
