﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanZoomCamera
struct LeanZoomCamera_t246347926;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanZoomCamera::.ctor()
extern "C"  void LeanZoomCamera__ctor_m2449201096 (LeanZoomCamera_t246347926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanZoomCamera::LateUpdate()
extern "C"  void LeanZoomCamera_LateUpdate_m2948504705 (LeanZoomCamera_t246347926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Lean.Touch.LeanZoomCamera::GetCurrent()
extern "C"  float LeanZoomCamera_GetCurrent_m2356110889 (LeanZoomCamera_t246347926 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanZoomCamera::SetCurrent(System.Single)
extern "C"  void LeanZoomCamera_SetCurrent_m2047454264 (LeanZoomCamera_t246347926 * __this, float ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
