﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanZoomCameraSmooth
struct LeanZoomCameraSmooth_t2278981112;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanZoomCameraSmooth::.ctor()
extern "C"  void LeanZoomCameraSmooth__ctor_m1881856166 (LeanZoomCameraSmooth_t2278981112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanZoomCameraSmooth::Start()
extern "C"  void LeanZoomCameraSmooth_Start_m2580194506 (LeanZoomCameraSmooth_t2278981112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanZoomCameraSmooth::LateUpdate()
extern "C"  void LeanZoomCameraSmooth_LateUpdate_m2348029071 (LeanZoomCameraSmooth_t2278981112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Lean.Touch.LeanZoomCameraSmooth::GetCurrent()
extern "C"  float LeanZoomCameraSmooth_GetCurrent_m2777750407 (LeanZoomCameraSmooth_t2278981112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanZoomCameraSmooth::SetCurrent(System.Single)
extern "C"  void LeanZoomCameraSmooth_SetCurrent_m543996198 (LeanZoomCameraSmooth_t2278981112 * __this, float ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
