﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AsyncOperation
struct AsyncOperation_t3814632279;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadingBar
struct  LoadingBar_t1246465185  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AsyncOperation LoadingBar::ao
	AsyncOperation_t3814632279 * ___ao_2;
	// UnityEngine.UI.Image LoadingBar::LoadingImage
	Image_t2042527209 * ___LoadingImage_3;
	// UnityEngine.UI.Text LoadingBar::LoadingText
	Text_t356221433 * ___LoadingText_4;

public:
	inline static int32_t get_offset_of_ao_2() { return static_cast<int32_t>(offsetof(LoadingBar_t1246465185, ___ao_2)); }
	inline AsyncOperation_t3814632279 * get_ao_2() const { return ___ao_2; }
	inline AsyncOperation_t3814632279 ** get_address_of_ao_2() { return &___ao_2; }
	inline void set_ao_2(AsyncOperation_t3814632279 * value)
	{
		___ao_2 = value;
		Il2CppCodeGenWriteBarrier(&___ao_2, value);
	}

	inline static int32_t get_offset_of_LoadingImage_3() { return static_cast<int32_t>(offsetof(LoadingBar_t1246465185, ___LoadingImage_3)); }
	inline Image_t2042527209 * get_LoadingImage_3() const { return ___LoadingImage_3; }
	inline Image_t2042527209 ** get_address_of_LoadingImage_3() { return &___LoadingImage_3; }
	inline void set_LoadingImage_3(Image_t2042527209 * value)
	{
		___LoadingImage_3 = value;
		Il2CppCodeGenWriteBarrier(&___LoadingImage_3, value);
	}

	inline static int32_t get_offset_of_LoadingText_4() { return static_cast<int32_t>(offsetof(LoadingBar_t1246465185, ___LoadingText_4)); }
	inline Text_t356221433 * get_LoadingText_4() const { return ___LoadingText_4; }
	inline Text_t356221433 ** get_address_of_LoadingText_4() { return &___LoadingText_4; }
	inline void set_LoadingText_4(Text_t356221433 * value)
	{
		___LoadingText_4 = value;
		Il2CppCodeGenWriteBarrier(&___LoadingText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
