﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NextsScence
struct NextsScence_t2555474679;

#include "codegen/il2cpp-codegen.h"

// System.Void NextsScence::.ctor()
extern "C"  void NextsScence__ctor_m1333748574 (NextsScence_t2555474679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NextsScence::Move()
extern "C"  void NextsScence_Move_m2313880475 (NextsScence_t2555474679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
