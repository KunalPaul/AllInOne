﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PanelButton
struct PanelButton_t4067832378;

#include "codegen/il2cpp-codegen.h"

// System.Void PanelButton::.ctor()
extern "C"  void PanelButton__ctor_m790107963 (PanelButton_t4067832378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelButton::Model()
extern "C"  void PanelButton_Model_m3069712300 (PanelButton_t4067832378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelButton::Video()
extern "C"  void PanelButton_Video_m3178075954 (PanelButton_t4067832378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PanelButton::TransparentVideo()
extern "C"  void PanelButton_TransparentVideo_m661804560 (PanelButton_t4067832378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
