﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReversedGameobject
struct ReversedGameobject_t1723523261;

#include "codegen/il2cpp-codegen.h"

// System.Void ReversedGameobject::.ctor()
extern "C"  void ReversedGameobject__ctor_m1658409430 (ReversedGameobject_t1723523261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReversedGameobject::Start()
extern "C"  void ReversedGameobject_Start_m1307351518 (ReversedGameobject_t1723523261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReversedGameobject::Update()
extern "C"  void ReversedGameobject_Update_m2566019805 (ReversedGameobject_t1723523261 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
