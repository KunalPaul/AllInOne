﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// loadlevel
struct loadlevel_t2580852430;

#include "codegen/il2cpp-codegen.h"

// System.Void loadlevel::.ctor()
extern "C"  void loadlevel__ctor_m2671948835 (loadlevel_t2580852430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void loadlevel::LOadlevel1()
extern "C"  void loadlevel_LOadlevel1_m2911843640 (loadlevel_t2580852430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
