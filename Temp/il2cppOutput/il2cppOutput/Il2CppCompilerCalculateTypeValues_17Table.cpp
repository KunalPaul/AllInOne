﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3090691518.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3929487414.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImp3510735303.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRendererImp4076072164.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnityImpl149264205.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceImpl3646117491.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainBuilder447373045.h"
#include "Vuforia_UnityExtensions_Vuforia_PropImpl1187075139.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTrackabl32254201.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke2474837022.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl211382711.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTrackerImpl_UpD944577254.h"
#include "Vuforia_UnityExtensions_Vuforia_TypeMapping254417876.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptor2106169489.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamTexAdaptorIm1817875757.h"
#include "Vuforia_UnityExtensions_Vuforia_WordImpl1843145168.h"
#include "Vuforia_UnityExtensions_Vuforia_WordPrefabCreation3171836134.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManagerImpl4282786523.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResultImpl911273601.h"
#include "Vuforia_UnityExtensions_Vuforia_WordListImpl2150426444.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNullWrapper3644069544.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaNativeWrapp2645113514.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaWrapper3750170617.h"
#include "Vuforia_UnityExtensions_Vuforia_ReconstructionAbst3509595417.h"
#include "Vuforia_UnityExtensions_Vuforia_PropAbstractBehavi1047177596.h"
#include "Vuforia_UnityExtensions_Vuforia_SmartTerrainTracke1462833936.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManager3369465942.h"
#include "Vuforia_UnityExtensions_Vuforia_StateManagerImpl3885489748.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl1380851697.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_T3807887646.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinderImpl_I2369108641.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSourceImp2574642394.h"
#include "Vuforia_UnityExtensions_Vuforia_TextureRenderer3312477626.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableImpl3421455115.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManagerImpl381223961.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonImpl2449737797.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamImpl2771725761.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile3757625748.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1724666488.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof3644865120.h"
#include "Vuforia_UnityExtensions_Vuforia_Image1391689025.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT3010530044.h"
#include "Vuforia_UnityExtensions_Vuforia_ImageTargetAbstrac3327552701.h"
#include "Vuforia_UnityExtensions_Vuforia_ObjectTracker1568044035.h"
#include "Vuforia_UnityExtensions_Vuforia_MaskOutAbstractBeh3489038957.h"
#include "Vuforia_UnityExtensions_Vuforia_MultiTargetAbstrac3616801211.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity657456673.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_InitE2149396216.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Vufor3491240575.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaUnity_Stora3897282321.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle4061728485.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaARControlle3506117492.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaMacros1884408435.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager2424874861.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManager_Tra1329355276.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer2933102835.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Fp1598668988.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4106934884.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi4137084396.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vec829768013.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Vi2617831468.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRenderer_Ren804170727.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil3083157244.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaRuntimeUtil1916387570.h"
#include "Vuforia_UnityExtensions_Vuforia_SurfaceUtilities4096327849.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder1347637805.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_InitStat4409649.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Updat1473252352.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Filte3082493643.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe1958726506.h"
#include "Vuforia_UnityExtensions_Vuforia_TextRecoAbstractBe2386081773.h"
#include "Vuforia_UnityExtensions_Vuforia_TextTracker89845299.h"
#include "Vuforia_UnityExtensions_Vuforia_SimpleTargetData3993525265.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour1779888572.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour4057911311.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour3993660444.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableSource2832298792.h"
#include "Vuforia_UnityExtensions_Vuforia_Tracker189438242.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackerManager308318605.h"
#include "Vuforia_UnityExtensions_Vuforia_TurnOffAbstractBeh4084926705.h"
#include "Vuforia_UnityExtensions_Vuforia_UserDefinedTargetB3589690572.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundAbst395384314.h"
#include "Vuforia_UnityExtensions_Vuforia_VideoBackgroundMan3515346924.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton3703236737.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButton_Sens1678924861.h"
#include "Vuforia_UnityExtensions_Vuforia_VirtualButtonAbstr2478279366.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (AutoRotationState_t3090691518)+ sizeof (Il2CppObject), sizeof(AutoRotationState_t3090691518_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[5] = 
{
	AutoRotationState_t3090691518::get_offset_of_setOnPause_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToPortrait_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToPortraitUpsideDown_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToLandscapeLeft_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	AutoRotationState_t3090691518::get_offset_of_autorotateToLandscapeRight_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (U3CU3Ec__DisplayClass86_0_t3929487414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1701[1] = 
{
	U3CU3Ec__DisplayClass86_0_t3929487414::get_offset_of_id_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (VuforiaRendererImpl_t3510735303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1702[6] = 
{
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBGConfig_1(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBGConfigSet_2(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mVideoBackgroundTexture_3(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mBackgroundTextureHasChanged_4(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mLastSetReflection_5(),
	VuforiaRendererImpl_t3510735303::get_offset_of_mNativeRenderingCallback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (RenderEvent_t4076072164)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1703[7] = 
{
	RenderEvent_t4076072164::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (VuforiaUnityImpl_t149264205), -1, sizeof(VuforiaUnityImpl_t149264205_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1704[5] = 
{
	0,
	0,
	0,
	VuforiaUnityImpl_t149264205_StaticFields::get_offset_of_mRendererDirty_3(),
	VuforiaUnityImpl_t149264205_StaticFields::get_offset_of_mWrapperType_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (SurfaceImpl_t3646117491), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[5] = 
{
	SurfaceImpl_t3646117491::get_offset_of_mNavMesh_7(),
	SurfaceImpl_t3646117491::get_offset_of_mMeshBoundaries_8(),
	SurfaceImpl_t3646117491::get_offset_of_mBoundingBox_9(),
	SurfaceImpl_t3646117491::get_offset_of_mSurfaceArea_10(),
	SurfaceImpl_t3646117491::get_offset_of_mAreaNeedsUpdate_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (SmartTerrainBuilderImpl_t447373045), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[2] = 
{
	SmartTerrainBuilderImpl_t447373045::get_offset_of_mReconstructionBehaviours_0(),
	SmartTerrainBuilderImpl_t447373045::get_offset_of_mIsInitialized_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (PropImpl_t1187075139), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1707[1] = 
{
	PropImpl_t1187075139::get_offset_of_mOrientedBoundingBox3D_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (SmartTerrainTrackableImpl_t32254201), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[5] = 
{
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mChildren_2(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mMesh_3(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mMeshRevision_4(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_mLocalPose_5(),
	SmartTerrainTrackableImpl_t32254201::get_offset_of_U3CParentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (SmartTerrainTrackerImpl_t2474837022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1709[2] = 
{
	SmartTerrainTrackerImpl_t2474837022::get_offset_of_mScaleToMillimeter_1(),
	SmartTerrainTrackerImpl_t2474837022::get_offset_of_mSmartTerrainBuilder_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (TextTrackerImpl_t211382711), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	TextTrackerImpl_t211382711::get_offset_of_mWordList_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (UpDirection_t944577254)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1711[5] = 
{
	UpDirection_t944577254::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (TypeMapping_t254417876), -1, sizeof(TypeMapping_t254417876_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1712[1] = 
{
	TypeMapping_t254417876_StaticFields::get_offset_of_sTypes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (WebCamTexAdaptor_t2106169489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (WebCamTexAdaptorImpl_t1817875757), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1714[2] = 
{
	WebCamTexAdaptorImpl_t1817875757::get_offset_of_mWebCamTexture_0(),
	WebCamTexAdaptorImpl_t1817875757::get_offset_of_mCheckCameraPermissions_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (WordImpl_t1843145168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1715[5] = 
{
	WordImpl_t1843145168::get_offset_of_mText_2(),
	WordImpl_t1843145168::get_offset_of_mSize_3(),
	WordImpl_t1843145168::get_offset_of_mLetterMask_4(),
	WordImpl_t1843145168::get_offset_of_mLetterImageHeader_5(),
	WordImpl_t1843145168::get_offset_of_mLetterBoundingBoxes_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (WordPrefabCreationMode_t3171836134)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1716[3] = 
{
	WordPrefabCreationMode_t3171836134::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (WordManagerImpl_t4282786523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1717[12] = 
{
	WordManagerImpl_t4282786523::get_offset_of_mTrackedWords_0(),
	WordManagerImpl_t4282786523::get_offset_of_mNewWords_1(),
	WordManagerImpl_t4282786523::get_offset_of_mLostWords_2(),
	WordManagerImpl_t4282786523::get_offset_of_mActiveWordBehaviours_3(),
	WordManagerImpl_t4282786523::get_offset_of_mWordBehavioursMarkedForDeletion_4(),
	WordManagerImpl_t4282786523::get_offset_of_mWaitingQueue_5(),
	0,
	WordManagerImpl_t4282786523::get_offset_of_mWordBehaviours_7(),
	WordManagerImpl_t4282786523::get_offset_of_mAutomaticTemplate_8(),
	WordManagerImpl_t4282786523::get_offset_of_mMaxInstances_9(),
	WordManagerImpl_t4282786523::get_offset_of_mWordPrefabCreationMode_10(),
	WordManagerImpl_t4282786523::get_offset_of_mVuforiaBehaviour_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (WordResultImpl_t911273601), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1718[5] = 
{
	WordResultImpl_t911273601::get_offset_of_mObb_0(),
	WordResultImpl_t911273601::get_offset_of_mPosition_1(),
	WordResultImpl_t911273601::get_offset_of_mOrientation_2(),
	WordResultImpl_t911273601::get_offset_of_mWord_3(),
	WordResultImpl_t911273601::get_offset_of_mStatus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (WordListImpl_t2150426444), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (VuforiaNullWrapper_t3644069544), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (VuforiaNativeWrapper_t2645113514), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (VuforiaWrapper_t3750170617), -1, sizeof(VuforiaWrapper_t3750170617_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[2] = 
{
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sWrapper_0(),
	VuforiaWrapper_t3750170617_StaticFields::get_offset_of_sCamIndependentWrapper_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (ReconstructionAbstractBehaviour_t3509595417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1730[22] = 
{
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mHasInitialized_2(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSmartTerrainEventHandlers_3(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnInitialized_4(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropCreated_5(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropUpdated_6(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnPropDeleted_7(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceCreated_8(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceUpdated_9(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mOnSurfaceDeleted_10(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mInitializedInEditor_11(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtentEnabled_12(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mMaximumExtent_13(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mAutomaticStart_14(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshUpdates_15(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mNavMeshPadding_16(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mReconstruction_17(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mSurfaces_18(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActiveSurfaceBehaviours_19(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mProps_20(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mActivePropBehaviours_21(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mPreviouslySetWorldCenterSurfaceTemplate_22(),
	ReconstructionAbstractBehaviour_t3509595417::get_offset_of_mIgnoreNextUpdate_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (PropAbstractBehaviour_t1047177596), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1732[2] = 
{
	PropAbstractBehaviour_t1047177596::get_offset_of_mProp_14(),
	PropAbstractBehaviour_t1047177596::get_offset_of_mBoxColliderToUpdate_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (SmartTerrainTracker_t1462833936), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (StateManager_t3369465942), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (StateManagerImpl_t3885489748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1736[9] = 
{
	StateManagerImpl_t3885489748::get_offset_of_mTrackableBehaviours_0(),
	StateManagerImpl_t3885489748::get_offset_of_mAutomaticallyCreatedBehaviours_1(),
	StateManagerImpl_t3885489748::get_offset_of_mBehavioursMarkedForDeletion_2(),
	StateManagerImpl_t3885489748::get_offset_of_mActiveTrackableBehaviours_3(),
	StateManagerImpl_t3885489748::get_offset_of_mWordManager_4(),
	StateManagerImpl_t3885489748::get_offset_of_mVuMarkManager_5(),
	StateManagerImpl_t3885489748::get_offset_of_mDeviceTrackingManager_6(),
	StateManagerImpl_t3885489748::get_offset_of_mCameraPositioningHelper_7(),
	StateManagerImpl_t3885489748::get_offset_of_mExtendedTrackingManager_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (TargetFinderImpl_t1380851697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[4] = 
{
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderStatePtr_0(),
	TargetFinderImpl_t1380851697::get_offset_of_mTargetFinderState_1(),
	TargetFinderImpl_t1380851697::get_offset_of_mNewResults_2(),
	TargetFinderImpl_t1380851697::get_offset_of_mImageTargets_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (TargetFinderState_t3807887646)+ sizeof (Il2CppObject), sizeof(TargetFinderState_t3807887646 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1738[4] = 
{
	TargetFinderState_t3807887646::get_offset_of_IsRequesting_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_UpdateState_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_ResultCount_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetFinderState_t3807887646::get_offset_of_unused_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (InternalTargetSearchResult_t2369108641)+ sizeof (Il2CppObject), sizeof(InternalTargetSearchResult_t2369108641 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1739[6] = 
{
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetNamePtr_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_UniqueTargetIdPtr_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_MetaDataPtr_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSearchResultPtr_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TargetSize_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	InternalTargetSearchResult_t2369108641::get_offset_of_TrackingRating_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (TrackableSourceImpl_t2574642394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[1] = 
{
	TrackableSourceImpl_t2574642394::get_offset_of_U3CTrackableSourcePtrU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (TextureRenderer_t3312477626), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1741[3] = 
{
	TextureRenderer_t3312477626::get_offset_of_mTextureBufferCamera_0(),
	TextureRenderer_t3312477626::get_offset_of_mTextureWidth_1(),
	TextureRenderer_t3312477626::get_offset_of_mTextureHeight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (TrackableImpl_t3421455115), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1742[2] = 
{
	TrackableImpl_t3421455115::get_offset_of_U3CNameU3Ek__BackingField_0(),
	TrackableImpl_t3421455115::get_offset_of_U3CIDU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (TrackerManagerImpl_t381223961), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1743[5] = 
{
	TrackerManagerImpl_t381223961::get_offset_of_mObjectTracker_1(),
	TrackerManagerImpl_t381223961::get_offset_of_mTextTracker_2(),
	TrackerManagerImpl_t381223961::get_offset_of_mSmartTerrainTracker_3(),
	TrackerManagerImpl_t381223961::get_offset_of_mDeviceTracker_4(),
	TrackerManagerImpl_t381223961::get_offset_of_mStateManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (VirtualButtonImpl_t2449737797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[6] = 
{
	VirtualButtonImpl_t2449737797::get_offset_of_mName_1(),
	VirtualButtonImpl_t2449737797::get_offset_of_mID_2(),
	VirtualButtonImpl_t2449737797::get_offset_of_mArea_3(),
	VirtualButtonImpl_t2449737797::get_offset_of_mIsEnabled_4(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentImageTarget_5(),
	VirtualButtonImpl_t2449737797::get_offset_of_mParentDataSet_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (WebCamImpl_t2771725761), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[15] = 
{
	WebCamImpl_t2771725761::get_offset_of_mARCameras_0(),
	WebCamImpl_t2771725761::get_offset_of_mOriginalCameraCullMask_1(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamTexture_2(),
	WebCamImpl_t2771725761::get_offset_of_mVideoModeData_3(),
	WebCamImpl_t2771725761::get_offset_of_mVideoTextureInfo_4(),
	WebCamImpl_t2771725761::get_offset_of_mTextureRenderer_5(),
	WebCamImpl_t2771725761::get_offset_of_mBufferReadTexture_6(),
	WebCamImpl_t2771725761::get_offset_of_mReadPixelsRect_7(),
	WebCamImpl_t2771725761::get_offset_of_mWebCamProfile_8(),
	WebCamImpl_t2771725761::get_offset_of_mFlipHorizontally_9(),
	WebCamImpl_t2771725761::get_offset_of_mIsDirty_10(),
	WebCamImpl_t2771725761::get_offset_of_mLastFrameIdx_11(),
	WebCamImpl_t2771725761::get_offset_of_mRenderTextureLayer_12(),
	WebCamImpl_t2771725761::get_offset_of_mWebcamPlaying_13(),
	WebCamImpl_t2771725761::get_offset_of_U3CIsTextureSizeAvailableU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (WebCamProfile_t3757625748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1746[1] = 
{
	WebCamProfile_t3757625748::get_offset_of_mProfileCollection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (ProfileData_t1724666488)+ sizeof (Il2CppObject), sizeof(ProfileData_t1724666488 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1747[3] = 
{
	ProfileData_t1724666488::get_offset_of_RequestedTextureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_ResampledTextureSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileData_t1724666488::get_offset_of_RequestedFPS_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (ProfileCollection_t3644865120)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[2] = 
{
	ProfileCollection_t3644865120::get_offset_of_DefaultProfile_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ProfileCollection_t3644865120::get_offset_of_Profiles_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (Image_t1391689025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (PIXEL_FORMAT_t3010530044)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1753[7] = 
{
	PIXEL_FORMAT_t3010530044::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (ImageTargetAbstractBehaviour_t3327552701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1754[8] = 
{
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mAspectRatio_20(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTargetType_21(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mWidth_22(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mHeight_23(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mImageTarget_24(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mVirtualButtonBehaviours_25(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastTransformScale_26(),
	ImageTargetAbstractBehaviour_t3327552701::get_offset_of_mLastSize_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (ObjectTracker_t1568044035), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (MaskOutAbstractBehaviour_t3489038957), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1756[1] = 
{
	MaskOutAbstractBehaviour_t3489038957::get_offset_of_maskMaterial_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (MultiTargetAbstractBehaviour_t3616801211), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1758[1] = 
{
	MultiTargetAbstractBehaviour_t3616801211::get_offset_of_mMultiTarget_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (VuforiaUnity_t657456673), -1, sizeof(VuforiaUnity_t657456673_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1759[1] = 
{
	VuforiaUnity_t657456673_StaticFields::get_offset_of_mHoloLensApiAbstraction_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (InitError_t2149396216)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1760[12] = 
{
	InitError_t2149396216::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (VuforiaHint_t3491240575)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1761[4] = 
{
	VuforiaHint_t3491240575::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (StorageType_t3897282321)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1762[4] = 
{
	StorageType_t3897282321::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (VuforiaARController_t4061728485), -1, sizeof(VuforiaARController_t4061728485_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1763[39] = 
{
	VuforiaARController_t4061728485::get_offset_of_CameraDeviceModeSetting_1(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousImageTargets_2(),
	VuforiaARController_t4061728485::get_offset_of_MaxSimultaneousObjectTargets_3(),
	VuforiaARController_t4061728485::get_offset_of_UseDelayedLoadingObjectTargets_4(),
	VuforiaARController_t4061728485::get_offset_of_CameraDirection_5(),
	VuforiaARController_t4061728485::get_offset_of_MirrorVideoBackground_6(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenterMode_7(),
	VuforiaARController_t4061728485::get_offset_of_mWorldCenter_8(),
	VuforiaARController_t4061728485::get_offset_of_mTrackerEventHandlers_9(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBgEventHandlers_10(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaInitialized_11(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaStarted_12(),
	VuforiaARController_t4061728485::get_offset_of_mOnVuforiaDeinitialized_13(),
	VuforiaARController_t4061728485::get_offset_of_mOnTrackablesUpdated_14(),
	VuforiaARController_t4061728485::get_offset_of_mRenderOnUpdate_15(),
	VuforiaARController_t4061728485::get_offset_of_mOnPause_16(),
	VuforiaARController_t4061728485::get_offset_of_mPaused_17(),
	VuforiaARController_t4061728485::get_offset_of_mOnBackgroundTextureChanged_18(),
	VuforiaARController_t4061728485::get_offset_of_mStartHasBeenInvoked_19(),
	VuforiaARController_t4061728485::get_offset_of_mHasStarted_20(),
	VuforiaARController_t4061728485::get_offset_of_mBackgroundTextureHasChanged_21(),
	VuforiaARController_t4061728485::get_offset_of_mCameraConfiguration_22(),
	VuforiaARController_t4061728485::get_offset_of_mEyewearBehaviour_23(),
	VuforiaARController_t4061728485::get_offset_of_mVideoBackgroundMgr_24(),
	VuforiaARController_t4061728485::get_offset_of_mCheckStopCamera_25(),
	VuforiaARController_t4061728485::get_offset_of_mClearMaterial_26(),
	VuforiaARController_t4061728485::get_offset_of_mMetalRendering_27(),
	VuforiaARController_t4061728485::get_offset_of_mHasStartedOnce_28(),
	VuforiaARController_t4061728485::get_offset_of_mWasEnabledBeforePause_29(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforePause_30(),
	VuforiaARController_t4061728485::get_offset_of_mObjectTrackerWasActiveBeforeDisabling_31(),
	VuforiaARController_t4061728485::get_offset_of_mLastUpdatedFrame_32(),
	VuforiaARController_t4061728485::get_offset_of_mTrackersRequestedToDeinit_33(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyLeftProjectionMatrix_34(),
	VuforiaARController_t4061728485::get_offset_of_mMissedToApplyRightProjectionMatrix_35(),
	VuforiaARController_t4061728485::get_offset_of_mLeftProjectMatrixToApply_36(),
	VuforiaARController_t4061728485::get_offset_of_mRightProjectMatrixToApply_37(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mInstance_38(),
	VuforiaARController_t4061728485_StaticFields::get_offset_of_mPadlock_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (WorldCenterMode_t3506117492)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1764[5] = 
{
	WorldCenterMode_t3506117492::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (VuforiaMacros_t1884408435)+ sizeof (Il2CppObject), sizeof(VuforiaMacros_t1884408435 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1765[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (VuforiaManager_t2424874861), -1, sizeof(VuforiaManager_t2424874861_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1766[1] = 
{
	VuforiaManager_t2424874861_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (TrackableIdPair_t1329355276)+ sizeof (Il2CppObject), sizeof(TrackableIdPair_t1329355276 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1767[2] = 
{
	TrackableIdPair_t1329355276::get_offset_of_TrackableId_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TrackableIdPair_t1329355276::get_offset_of_ResultId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (VuforiaRenderer_t2933102835), -1, sizeof(VuforiaRenderer_t2933102835_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1768[1] = 
{
	VuforiaRenderer_t2933102835_StaticFields::get_offset_of_sInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (FpsHint_t1598668988)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1769[5] = 
{
	FpsHint_t1598668988::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (VideoBackgroundReflection_t4106934884)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1770[4] = 
{
	VideoBackgroundReflection_t4106934884::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (VideoBGCfgData_t4137084396)+ sizeof (Il2CppObject), sizeof(VideoBGCfgData_t4137084396 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1771[4] = 
{
	VideoBGCfgData_t4137084396::get_offset_of_position_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_size_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_enabled_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoBGCfgData_t4137084396::get_offset_of_reflectionInteger_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (Vec2I_t829768013)+ sizeof (Il2CppObject), sizeof(Vec2I_t829768013 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1772[2] = 
{
	Vec2I_t829768013::get_offset_of_x_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vec2I_t829768013::get_offset_of_y_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (VideoTextureInfo_t2617831468)+ sizeof (Il2CppObject), sizeof(VideoTextureInfo_t2617831468 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[2] = 
{
	VideoTextureInfo_t2617831468::get_offset_of_textureSize_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VideoTextureInfo_t2617831468::get_offset_of_imageSize_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (RendererAPI_t804170727)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1774[5] = 
{
	RendererAPI_t804170727::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (VuforiaRuntimeUtilities_t3083157244), -1, sizeof(VuforiaRuntimeUtilities_t3083157244_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1775[2] = 
{
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sWebCamUsed_0(),
	VuforiaRuntimeUtilities_t3083157244_StaticFields::get_offset_of_sNativePluginSupport_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (InitializableBool_t1916387570)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1776[4] = 
{
	InitializableBool_t1916387570::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (SurfaceUtilities_t4096327849), -1, sizeof(SurfaceUtilities_t4096327849_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1777[1] = 
{
	SurfaceUtilities_t4096327849_StaticFields::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (TargetFinder_t1347637805), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (InitState_t4409649)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1779[6] = 
{
	InitState_t4409649::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (UpdateState_t1473252352)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1780[12] = 
{
	UpdateState_t1473252352::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (FilterMode_t3082493643)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1781[3] = 
{
	FilterMode_t3082493643::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (TargetSearchResult_t1958726506)+ sizeof (Il2CppObject), sizeof(TargetSearchResult_t1958726506_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1782[6] = 
{
	TargetSearchResult_t1958726506::get_offset_of_TargetName_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_UniqueTargetId_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSize_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_MetaData_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TrackingRating_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	TargetSearchResult_t1958726506::get_offset_of_TargetSearchResultPtr_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (TextRecoAbstractBehaviour_t2386081773), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[12] = 
{
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mHasInitialized_2(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforePause_3(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTrackerWasActiveBeforeDisabling_4(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordListFile_5(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mCustomWordListFile_6(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalCustomWords_7(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterMode_8(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mFilterListFile_9(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mAdditionalFilterWords_10(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mWordPrefabCreationMode_11(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mMaximumWordInstances_12(),
	TextRecoAbstractBehaviour_t2386081773::get_offset_of_mTextRecoEventHandlers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (TextTracker_t89845299), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (SimpleTargetData_t3993525265)+ sizeof (Il2CppObject), sizeof(SimpleTargetData_t3993525265 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1785[2] = 
{
	SimpleTargetData_t3993525265::get_offset_of_id_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SimpleTargetData_t3993525265::get_offset_of_unused_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (TrackableBehaviour_t1779888572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1787[8] = 
{
	TrackableBehaviour_t1779888572::get_offset_of_U3CTimeStampU3Ek__BackingField_2(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableName_3(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreserveChildSize_4(),
	TrackableBehaviour_t1779888572::get_offset_of_mInitializedInEditor_5(),
	TrackableBehaviour_t1779888572::get_offset_of_mPreviousScale_6(),
	TrackableBehaviour_t1779888572::get_offset_of_mStatus_7(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackable_8(),
	TrackableBehaviour_t1779888572::get_offset_of_mTrackableEventHandlers_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (Status_t4057911311)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1788[7] = 
{
	Status_t4057911311::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (CoordinateSystem_t3993660444)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1789[4] = 
{
	CoordinateSystem_t3993660444::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { sizeof (TrackableSource_t2832298792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (Tracker_t189438242), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1791[1] = 
{
	Tracker_t189438242::get_offset_of_U3CIsActiveU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (TrackerManager_t308318605), -1, sizeof(TrackerManager_t308318605_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1792[1] = 
{
	TrackerManager_t308318605_StaticFields::get_offset_of_mInstance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { sizeof (TurnOffAbstractBehaviour_t4084926705), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (UserDefinedTargetBuildingAbstractBehaviour_t3589690572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1794[11] = 
{
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mObjectTracker_2(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mLastFrameQuality_3(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyScanning_4(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasScanningBeforeDisable_5(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mCurrentlyBuilding_6(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mWasBuildingBeforeDisable_7(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mOnInitializedCalled_8(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_mHandlers_9(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopTrackerWhileScanning_10(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StartScanningAutomatically_11(),
	UserDefinedTargetBuildingAbstractBehaviour_t3589690572::get_offset_of_StopScanningWhenFinshedBuilding_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (VideoBackgroundAbstractBehaviour_t395384314), -1, sizeof(VideoBackgroundAbstractBehaviour_t395384314_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1795[12] = 
{
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mClearBuffers_2(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mSkipStateUpdates_3(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaARController_4(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCamera_5(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mBackgroundBehaviour_6(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mStereoDepth_7(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mFrameCounter_8(),
	VideoBackgroundAbstractBehaviour_t395384314_StaticFields::get_offset_of_mRenderCounter_9(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mResetMatrix_10(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mVuforiaFrustumSkew_11(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mCenterToEyeAxis_12(),
	VideoBackgroundAbstractBehaviour_t395384314::get_offset_of_mDisabledMeshRenderers_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (VideoBackgroundManager_t3515346924), -1, sizeof(VideoBackgroundManager_t3515346924_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1796[8] = 
{
	VideoBackgroundManager_t3515346924::get_offset_of_mClippingMode_1(),
	VideoBackgroundManager_t3515346924::get_offset_of_mMatteShader_2(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBackgroundEnabled_3(),
	VideoBackgroundManager_t3515346924::get_offset_of_mTexture_4(),
	VideoBackgroundManager_t3515346924::get_offset_of_mVideoBgConfigChanged_5(),
	VideoBackgroundManager_t3515346924::get_offset_of_mNativeTexturePtr_6(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mInstance_7(),
	VideoBackgroundManager_t3515346924_StaticFields::get_offset_of_mPadlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (VirtualButton_t3703236737), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1797[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (Sensitivity_t1678924861)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1798[4] = 
{
	Sensitivity_t1678924861::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (VirtualButtonAbstractBehaviour_t2478279366), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1799[15] = 
{
	0,
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mName_3(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivity_4(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHasUpdatedPose_5(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevTransform_6(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPrevParent_7(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mSensitivityDirty_8(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviousSensitivity_9(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPreviouslyEnabled_10(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mPressed_11(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mHandlers_12(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mLeftTop_13(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mRightBottom_14(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mUnregisterOnDestroy_15(),
	VirtualButtonAbstractBehaviour_t2478279366::get_offset_of_mVirtualButton_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
