﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamARController2804466264.h"
#include "Vuforia_UnityExtensions_Vuforia_WordManager1585193471.h"
#include "Vuforia_UnityExtensions_Vuforia_WordResult1915507197.h"
#include "Vuforia_UnityExtensions_Vuforia_WordTemplateMode1097144495.h"
#include "Vuforia_UnityExtensions_Vuforia_WordAbstractBehavi2878458725.h"
#include "Vuforia_UnityExtensions_Vuforia_WordFilterMode695600879.h"
#include "Vuforia_UnityExtensions_Vuforia_WordList1278495262.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearCalibration2396922556.h"
#include "Vuforia_UnityExtensions_Vuforia_EyewearUserCalibrat626398268.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDe1486305137.h"
#include "Vuforia_UnityExtensions_U3CPrivateImplementationDet978476007.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg1967201810.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3959312622.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg3365010046.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventTrigg2524067914.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve1693084770.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirect1406276862.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycasterM3179336627.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_UIBehaviou3960014691.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD1524870173.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AbstractEv1333959294.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2681005625.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1599784723.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve2981963041.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1414739712.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInput621514313.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseInputM1295781545.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp1441575871.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp2688375492.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3572864619.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInp3709210170.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneIn70867863.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Standalone2680906638.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInput2561058385.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseRaycas2336171397.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_Physics2DR3236822917.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PhysicsRayc249603239.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3438117476.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color1328781136.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Color3293839588.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2986189219.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_Float2824271922.h"
#include "UnityEngine_UI_UnityEngine_UI_AnimationTriggers3244928895.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_ButtonClicked2455055323.h"
#include "UnityEngine_UI_UnityEngine_UI_Button_U3COnFinishSubm99135383.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdate1528800019.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasUpdateRegistry1780385998.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock2652774230.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls1409660779.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Reso2975512894.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown1985816271.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownIte4139978805.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionData2420267500.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_OptionDataL2653737080.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_DropdownEve2203087800.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CShowU3Ec2089497532.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown_U3CDelayedD3461999471.h"
#include "UnityEngine_UI_UnityEngine_UI_FontData2614388407.h"
#include "UnityEngine_UI_UnityEngine_UI_FontUpdateTracker2633059652.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic2426225576.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster410733016.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_Blo2548930813.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRegistry377833367.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type3352948571.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod1640962579.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginHorizont1880137149.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_OriginVertical3595376133.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin902486598028.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Origin1803744816572.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (WebCamARController_t2804466264), -1, sizeof(WebCamARController_t2804466264_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1800[6] = 
{
	WebCamARController_t2804466264::get_offset_of_RenderTextureLayer_1(),
	WebCamARController_t2804466264::get_offset_of_mDeviceNameSetInEditor_2(),
	WebCamARController_t2804466264::get_offset_of_mFlipHorizontally_3(),
	WebCamARController_t2804466264::get_offset_of_mWebCamImpl_4(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mInstance_5(),
	WebCamARController_t2804466264_StaticFields::get_offset_of_mPadlock_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (WordManager_t1585193471), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (WordResult_t1915507197), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (WordTemplateMode_t1097144495)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1804[3] = 
{
	WordTemplateMode_t1097144495::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (WordAbstractBehaviour_t2878458725), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[3] = 
{
	WordAbstractBehaviour_t2878458725::get_offset_of_mMode_10(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mSpecificWord_11(),
	WordAbstractBehaviour_t2878458725::get_offset_of_mWord_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (WordFilterMode_t695600879)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[4] = 
{
	WordFilterMode_t695600879::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (WordList_t1278495262), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (EyewearCalibrationProfileManager_t2396922556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (EyewearUserCalibrator_t626398268), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1810[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U3898C2022A0C02FCE602BF05E1C09BD48301606E5_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (__StaticArrayInitTypeSizeU3D24_t978476007)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D24_t978476007 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1813[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1832[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (EventTrigger_t1967201810), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[2] = 
{
	EventTrigger_t1967201810::get_offset_of_m_Delegates_2(),
	EventTrigger_t1967201810::get_offset_of_delegates_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (TriggerEvent_t3959312622), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (Entry_t3365010046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1835[2] = 
{
	Entry_t3365010046::get_offset_of_eventID_0(),
	Entry_t3365010046::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (EventTriggerType_t2524067914)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[18] = 
{
	EventTriggerType_t2524067914::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (ExecuteEvents_t1693084770), -1, sizeof(ExecuteEvents_t1693084770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1837[36] = 
{
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerEnterHandler_0(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerExitHandler_1(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerDownHandler_2(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerUpHandler_3(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_PointerClickHandler_4(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InitializePotentialDragHandler_5(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_BeginDragHandler_6(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DragHandler_7(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_EndDragHandler_8(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DropHandler_9(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_ScrollHandler_10(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_UpdateSelectedHandler_11(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SelectHandler_12(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_DeselectHandler_13(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_MoveHandler_14(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_SubmitHandler_15(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_CancelHandler_16(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_HandlerListPool_17(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_s_InternalTransformList_18(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_19(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_20(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache2_21(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache3_22(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache4_23(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache5_24(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache6_25(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache7_26(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache8_27(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache9_28(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheA_29(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheB_30(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheC_31(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheD_32(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheE_33(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cacheF_34(),
	ExecuteEvents_t1693084770_StaticFields::get_offset_of_U3CU3Ef__mgU24cache10_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (MoveDirection_t1406276862)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1839[6] = 
{
	MoveDirection_t1406276862::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (RaycasterManager_t3179336627), -1, sizeof(RaycasterManager_t3179336627_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1840[1] = 
{
	RaycasterManager_t3179336627_StaticFields::get_offset_of_s_Raycasters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (RaycastResult_t21186376)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[10] = 
{
	RaycastResult_t21186376::get_offset_of_m_GameObject_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_module_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_distance_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_index_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_depth_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingLayer_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_sortingOrder_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldPosition_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_worldNormal_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	RaycastResult_t21186376::get_offset_of_screenPosition_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (UIBehaviour_t3960014691), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (AxisEventData_t1524870173), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[2] = 
{
	AxisEventData_t1524870173::get_offset_of_U3CmoveVectorU3Ek__BackingField_2(),
	AxisEventData_t1524870173::get_offset_of_U3CmoveDirU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (AbstractEventData_t1333959294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[1] = 
{
	AbstractEventData_t1333959294::get_offset_of_m_Used_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (BaseEventData_t2681005625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[1] = 
{
	BaseEventData_t2681005625::get_offset_of_m_EventSystem_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (PointerEventData_t1599784723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[21] = 
{
	PointerEventData_t1599784723::get_offset_of_U3CpointerEnterU3Ek__BackingField_2(),
	PointerEventData_t1599784723::get_offset_of_m_PointerPress_3(),
	PointerEventData_t1599784723::get_offset_of_U3ClastPressU3Ek__BackingField_4(),
	PointerEventData_t1599784723::get_offset_of_U3CrawPointerPressU3Ek__BackingField_5(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerDragU3Ek__BackingField_6(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerCurrentRaycastU3Ek__BackingField_7(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerPressRaycastU3Ek__BackingField_8(),
	PointerEventData_t1599784723::get_offset_of_hovered_9(),
	PointerEventData_t1599784723::get_offset_of_U3CeligibleForClickU3Ek__BackingField_10(),
	PointerEventData_t1599784723::get_offset_of_U3CpointerIdU3Ek__BackingField_11(),
	PointerEventData_t1599784723::get_offset_of_U3CpositionU3Ek__BackingField_12(),
	PointerEventData_t1599784723::get_offset_of_U3CdeltaU3Ek__BackingField_13(),
	PointerEventData_t1599784723::get_offset_of_U3CpressPositionU3Ek__BackingField_14(),
	PointerEventData_t1599784723::get_offset_of_U3CworldPositionU3Ek__BackingField_15(),
	PointerEventData_t1599784723::get_offset_of_U3CworldNormalU3Ek__BackingField_16(),
	PointerEventData_t1599784723::get_offset_of_U3CclickTimeU3Ek__BackingField_17(),
	PointerEventData_t1599784723::get_offset_of_U3CclickCountU3Ek__BackingField_18(),
	PointerEventData_t1599784723::get_offset_of_U3CscrollDeltaU3Ek__BackingField_19(),
	PointerEventData_t1599784723::get_offset_of_U3CuseDragThresholdU3Ek__BackingField_20(),
	PointerEventData_t1599784723::get_offset_of_U3CdraggingU3Ek__BackingField_21(),
	PointerEventData_t1599784723::get_offset_of_U3CbuttonU3Ek__BackingField_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (InputButton_t2981963041)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1847[4] = 
{
	InputButton_t2981963041::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (FramePressState_t1414739712)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1848[5] = 
{
	FramePressState_t1414739712::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (BaseInput_t621514313), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (BaseInputModule_t1295781545), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[6] = 
{
	BaseInputModule_t1295781545::get_offset_of_m_RaycastResultCache_2(),
	BaseInputModule_t1295781545::get_offset_of_m_AxisEventData_3(),
	BaseInputModule_t1295781545::get_offset_of_m_EventSystem_4(),
	BaseInputModule_t1295781545::get_offset_of_m_BaseEventData_5(),
	BaseInputModule_t1295781545::get_offset_of_m_InputOverride_6(),
	BaseInputModule_t1295781545::get_offset_of_m_DefaultInput_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (PointerInputModule_t1441575871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1851[6] = 
{
	0,
	0,
	0,
	0,
	PointerInputModule_t1441575871::get_offset_of_m_PointerData_12(),
	PointerInputModule_t1441575871::get_offset_of_m_MouseState_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (ButtonState_t2688375492), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[2] = 
{
	ButtonState_t2688375492::get_offset_of_m_Button_0(),
	ButtonState_t2688375492::get_offset_of_m_EventData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (MouseState_t3572864619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1853[1] = 
{
	MouseState_t3572864619::get_offset_of_m_TrackedButtons_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (MouseButtonEventData_t3709210170), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1854[2] = 
{
	MouseButtonEventData_t3709210170::get_offset_of_buttonState_0(),
	MouseButtonEventData_t3709210170::get_offset_of_buttonData_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (StandaloneInputModule_t70867863), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1855[13] = 
{
	StandaloneInputModule_t70867863::get_offset_of_m_PrevActionTime_14(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMoveVector_15(),
	StandaloneInputModule_t70867863::get_offset_of_m_ConsecutiveMoveCount_16(),
	StandaloneInputModule_t70867863::get_offset_of_m_LastMousePosition_17(),
	StandaloneInputModule_t70867863::get_offset_of_m_MousePosition_18(),
	StandaloneInputModule_t70867863::get_offset_of_m_CurrentFocusedGameObject_19(),
	StandaloneInputModule_t70867863::get_offset_of_m_HorizontalAxis_20(),
	StandaloneInputModule_t70867863::get_offset_of_m_VerticalAxis_21(),
	StandaloneInputModule_t70867863::get_offset_of_m_SubmitButton_22(),
	StandaloneInputModule_t70867863::get_offset_of_m_CancelButton_23(),
	StandaloneInputModule_t70867863::get_offset_of_m_InputActionsPerSecond_24(),
	StandaloneInputModule_t70867863::get_offset_of_m_RepeatDelay_25(),
	StandaloneInputModule_t70867863::get_offset_of_m_ForceModuleActive_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (InputMode_t2680906638)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1856[3] = 
{
	InputMode_t2680906638::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (TouchInputModule_t2561058385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[3] = 
{
	TouchInputModule_t2561058385::get_offset_of_m_LastMousePosition_14(),
	TouchInputModule_t2561058385::get_offset_of_m_MousePosition_15(),
	TouchInputModule_t2561058385::get_offset_of_m_ForceModuleActive_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (BaseRaycaster_t2336171397), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (Physics2DRaycaster_t3236822917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (PhysicsRaycaster_t249603239), -1, sizeof(PhysicsRaycaster_t249603239_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1860[4] = 
{
	0,
	PhysicsRaycaster_t249603239::get_offset_of_m_EventCamera_3(),
	PhysicsRaycaster_t249603239::get_offset_of_m_EventMask_4(),
	PhysicsRaycaster_t249603239_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (ColorTween_t3438117476)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[6] = 
{
	ColorTween_t3438117476::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_StartColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TargetColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_TweenMode_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_Duration_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorTween_t3438117476::get_offset_of_m_IgnoreTimeScale_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (ColorTweenMode_t1328781136)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1863[4] = 
{
	ColorTweenMode_t1328781136::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (ColorTweenCallback_t3293839588), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (FloatTween_t2986189219)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[5] = 
{
	FloatTween_t2986189219::get_offset_of_m_Target_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_StartValue_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_TargetValue_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_Duration_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	FloatTween_t2986189219::get_offset_of_m_IgnoreTimeScale_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (FloatTweenCallback_t2824271922), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[6] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (AnimationTriggers_t3244928895), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[8] = 
{
	0,
	0,
	0,
	0,
	AnimationTriggers_t3244928895::get_offset_of_m_NormalTrigger_4(),
	AnimationTriggers_t3244928895::get_offset_of_m_HighlightedTrigger_5(),
	AnimationTriggers_t3244928895::get_offset_of_m_PressedTrigger_6(),
	AnimationTriggers_t3244928895::get_offset_of_m_DisabledTrigger_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (Button_t2872111280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[1] = 
{
	Button_t2872111280::get_offset_of_m_OnClick_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (ButtonClickedEvent_t2455055323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (U3COnFinishSubmitU3Ec__Iterator0_t99135383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[6] = 
{
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U3CfadeTimeU3E__0_0(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U3CelapsedTimeU3E__1_1(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24this_2(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24current_3(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24disposing_4(),
	U3COnFinishSubmitU3Ec__Iterator0_t99135383::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (CanvasUpdate_t1528800019)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1873[7] = 
{
	CanvasUpdate_t1528800019::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (CanvasUpdateRegistry_t1780385998), -1, sizeof(CanvasUpdateRegistry_t1780385998_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1875[7] = 
{
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_s_Instance_0(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_PerformingLayoutUpdate_1(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_PerformingGraphicUpdate_2(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_LayoutRebuildQueue_3(),
	CanvasUpdateRegistry_t1780385998::get_offset_of_m_GraphicRebuildQueue_4(),
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_s_SortLayoutFunction_5(),
	CanvasUpdateRegistry_t1780385998_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (ColorBlock_t2652774230)+ sizeof (Il2CppObject), sizeof(ColorBlock_t2652774230 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1876[6] = 
{
	ColorBlock_t2652774230::get_offset_of_m_NormalColor_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_HighlightedColor_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_PressedColor_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_DisabledColor_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_ColorMultiplier_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ColorBlock_t2652774230::get_offset_of_m_FadeDuration_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (DefaultControls_t1409660779), -1, sizeof(DefaultControls_t1409660779_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1877[9] = 
{
	0,
	0,
	0,
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ThickElementSize_3(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ThinElementSize_4(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_ImageElementSize_5(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_DefaultSelectableColor_6(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_PanelColor_7(),
	DefaultControls_t1409660779_StaticFields::get_offset_of_s_TextColor_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (Resources_t2975512894)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[7] = 
{
	Resources_t2975512894::get_offset_of_standard_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_background_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_inputField_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_knob_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_checkmark_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_dropdown_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Resources_t2975512894::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (Dropdown_t1985816271), -1, sizeof(Dropdown_t1985816271_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1879[14] = 
{
	Dropdown_t1985816271::get_offset_of_m_Template_16(),
	Dropdown_t1985816271::get_offset_of_m_CaptionText_17(),
	Dropdown_t1985816271::get_offset_of_m_CaptionImage_18(),
	Dropdown_t1985816271::get_offset_of_m_ItemText_19(),
	Dropdown_t1985816271::get_offset_of_m_ItemImage_20(),
	Dropdown_t1985816271::get_offset_of_m_Value_21(),
	Dropdown_t1985816271::get_offset_of_m_Options_22(),
	Dropdown_t1985816271::get_offset_of_m_OnValueChanged_23(),
	Dropdown_t1985816271::get_offset_of_m_Dropdown_24(),
	Dropdown_t1985816271::get_offset_of_m_Blocker_25(),
	Dropdown_t1985816271::get_offset_of_m_Items_26(),
	Dropdown_t1985816271::get_offset_of_m_AlphaTweenRunner_27(),
	Dropdown_t1985816271::get_offset_of_validTemplate_28(),
	Dropdown_t1985816271_StaticFields::get_offset_of_s_NoOptionData_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (DropdownItem_t4139978805), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[4] = 
{
	DropdownItem_t4139978805::get_offset_of_m_Text_2(),
	DropdownItem_t4139978805::get_offset_of_m_Image_3(),
	DropdownItem_t4139978805::get_offset_of_m_RectTransform_4(),
	DropdownItem_t4139978805::get_offset_of_m_Toggle_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (OptionData_t2420267500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[2] = 
{
	OptionData_t2420267500::get_offset_of_m_Text_0(),
	OptionData_t2420267500::get_offset_of_m_Image_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (OptionDataList_t2653737080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[1] = 
{
	OptionDataList_t2653737080::get_offset_of_m_Options_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (DropdownEvent_t2203087800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (U3CShowU3Ec__AnonStorey1_t2089497532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1884[2] = 
{
	U3CShowU3Ec__AnonStorey1_t2089497532::get_offset_of_item_0(),
	U3CShowU3Ec__AnonStorey1_t2089497532::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3461999471), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[5] = 
{
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3461999471::get_offset_of_delay_0(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3461999471::get_offset_of_U24this_1(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3461999471::get_offset_of_U24current_2(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3461999471::get_offset_of_U24disposing_3(),
	U3CDelayedDestroyDropdownListU3Ec__Iterator0_t3461999471::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (FontData_t2614388407), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[12] = 
{
	FontData_t2614388407::get_offset_of_m_Font_0(),
	FontData_t2614388407::get_offset_of_m_FontSize_1(),
	FontData_t2614388407::get_offset_of_m_FontStyle_2(),
	FontData_t2614388407::get_offset_of_m_BestFit_3(),
	FontData_t2614388407::get_offset_of_m_MinSize_4(),
	FontData_t2614388407::get_offset_of_m_MaxSize_5(),
	FontData_t2614388407::get_offset_of_m_Alignment_6(),
	FontData_t2614388407::get_offset_of_m_AlignByGeometry_7(),
	FontData_t2614388407::get_offset_of_m_RichText_8(),
	FontData_t2614388407::get_offset_of_m_HorizontalOverflow_9(),
	FontData_t2614388407::get_offset_of_m_VerticalOverflow_10(),
	FontData_t2614388407::get_offset_of_m_LineSpacing_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (FontUpdateTracker_t2633059652), -1, sizeof(FontUpdateTracker_t2633059652_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1887[3] = 
{
	FontUpdateTracker_t2633059652_StaticFields::get_offset_of_m_Tracked_0(),
	FontUpdateTracker_t2633059652_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_1(),
	FontUpdateTracker_t2633059652_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (Graphic_t2426225576), -1, sizeof(Graphic_t2426225576_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1888[17] = 
{
	Graphic_t2426225576_StaticFields::get_offset_of_s_DefaultUI_2(),
	Graphic_t2426225576_StaticFields::get_offset_of_s_WhiteTexture_3(),
	Graphic_t2426225576::get_offset_of_m_Material_4(),
	Graphic_t2426225576::get_offset_of_m_Color_5(),
	Graphic_t2426225576::get_offset_of_m_RaycastTarget_6(),
	Graphic_t2426225576::get_offset_of_m_RectTransform_7(),
	Graphic_t2426225576::get_offset_of_m_CanvasRender_8(),
	Graphic_t2426225576::get_offset_of_m_Canvas_9(),
	Graphic_t2426225576::get_offset_of_m_VertsDirty_10(),
	Graphic_t2426225576::get_offset_of_m_MaterialDirty_11(),
	Graphic_t2426225576::get_offset_of_m_OnDirtyLayoutCallback_12(),
	Graphic_t2426225576::get_offset_of_m_OnDirtyVertsCallback_13(),
	Graphic_t2426225576::get_offset_of_m_OnDirtyMaterialCallback_14(),
	Graphic_t2426225576_StaticFields::get_offset_of_s_Mesh_15(),
	Graphic_t2426225576_StaticFields::get_offset_of_s_VertexHelper_16(),
	Graphic_t2426225576::get_offset_of_m_ColorTweenRunner_17(),
	Graphic_t2426225576::get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (GraphicRaycaster_t410733016), -1, sizeof(GraphicRaycaster_t410733016_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1889[8] = 
{
	0,
	GraphicRaycaster_t410733016::get_offset_of_m_IgnoreReversedGraphics_3(),
	GraphicRaycaster_t410733016::get_offset_of_m_BlockingObjects_4(),
	GraphicRaycaster_t410733016::get_offset_of_m_BlockingMask_5(),
	GraphicRaycaster_t410733016::get_offset_of_m_Canvas_6(),
	GraphicRaycaster_t410733016::get_offset_of_m_RaycastResults_7(),
	GraphicRaycaster_t410733016_StaticFields::get_offset_of_s_SortedGraphics_8(),
	GraphicRaycaster_t410733016_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (BlockingObjects_t2548930813)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1890[5] = 
{
	BlockingObjects_t2548930813::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (GraphicRegistry_t377833367), -1, sizeof(GraphicRegistry_t377833367_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1891[3] = 
{
	GraphicRegistry_t377833367_StaticFields::get_offset_of_s_Instance_0(),
	GraphicRegistry_t377833367::get_offset_of_m_Graphics_1(),
	GraphicRegistry_t377833367_StaticFields::get_offset_of_s_EmptyList_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (Image_t2042527209), -1, sizeof(Image_t2042527209_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1893[15] = 
{
	Image_t2042527209_StaticFields::get_offset_of_s_ETC1DefaultUI_28(),
	Image_t2042527209::get_offset_of_m_Sprite_29(),
	Image_t2042527209::get_offset_of_m_OverrideSprite_30(),
	Image_t2042527209::get_offset_of_m_Type_31(),
	Image_t2042527209::get_offset_of_m_PreserveAspect_32(),
	Image_t2042527209::get_offset_of_m_FillCenter_33(),
	Image_t2042527209::get_offset_of_m_FillMethod_34(),
	Image_t2042527209::get_offset_of_m_FillAmount_35(),
	Image_t2042527209::get_offset_of_m_FillClockwise_36(),
	Image_t2042527209::get_offset_of_m_FillOrigin_37(),
	Image_t2042527209::get_offset_of_m_AlphaHitTestMinimumThreshold_38(),
	Image_t2042527209_StaticFields::get_offset_of_s_VertScratch_39(),
	Image_t2042527209_StaticFields::get_offset_of_s_UVScratch_40(),
	Image_t2042527209_StaticFields::get_offset_of_s_Xy_41(),
	Image_t2042527209_StaticFields::get_offset_of_s_Uv_42(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (Type_t3352948571)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1894[5] = 
{
	Type_t3352948571::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (FillMethod_t1640962579)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1895[6] = 
{
	FillMethod_t1640962579::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (OriginHorizontal_t1880137149)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1896[3] = 
{
	OriginHorizontal_t1880137149::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (OriginVertical_t3595376133)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1897[3] = 
{
	OriginVertical_t3595376133::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (Origin90_t2486598028)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[5] = 
{
	Origin90_t2486598028::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (Origin180_t3744816572)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1899[5] = 
{
	Origin180_t3744816572::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
