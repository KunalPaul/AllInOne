﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CameraSettings3536359094.h"
#include "AssemblyU2DCSharp_CameraSettings_U3CRestoreOrigina3562025758.h"
#include "AssemblyU2DCSharp_FrameRateSettings1863881954.h"
#include "AssemblyU2DCSharp_InitErrorHandler1791388012.h"
#include "AssemblyU2DCSharp_MenuAnimator2049002970.h"
#include "AssemblyU2DCSharp_MenuOptions3210604277.h"
#include "AssemblyU2DCSharp_AboutScreen3562380015.h"
#include "AssemblyU2DCSharp_AsyncSceneLoader2733707743.h"
#include "AssemblyU2DCSharp_AsyncSceneLoader_U3CLoadNextScen1824579776.h"
#include "AssemblyU2DCSharp_LoadingScreen2880017196.h"
#include "AssemblyU2DCSharp_TapHandler3409799063.h"
#include "AssemblyU2DCSharp_TrackableSettings4265251850.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanDestroy3536643294.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanDragLine550158934.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanDragTrail3533323662.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanDragTrail_Link390936538.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerDown1453848917.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerDown_LeanFi4217027252.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld681037536.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld_Finger1929589405.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerHeld_Link182636800.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerSwipe3716445899.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerSwipe_Finge1048019214.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerTap2593896000.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerTap_LeanFin1916295887.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerUp2387037340.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFingerUp_LeanFing2500729307.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanMultiTap2693737624.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanMultiTap_IntEvent18007207.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanMultiTapInfo4075083494.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanOpenUrl3701782623.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPan644466001.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPanSmooth2363787483.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPressSelect4275113321.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPressSelect_Selec1810901247.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanPressSelect_Searc3326327913.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanRotate827445339.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanScale2479469360.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect3606489640.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect_SelectType2074988034.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect_SearchType3831610778.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelect_ReselectTyp399732767.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectable3692576450.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectable_LeanFi2546124889.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableBehavio2782920709.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableRenderer652725566.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableSpriteR1641095051.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableTransla2273971066.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSelectableTransla3840055007.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSpawnGameObject1465354280.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeDirection43774896743.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeDirection81449297915.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody2D1364749039.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody2DN2933744255.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody3D4093632394.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSwipeRigidbody3DN3590775226.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTapSelect4283262707.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTouchEvents41969022.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTranslate3292440434.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTranslateSmooth748462924.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanZoomCamera246347926.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanZoomCameraSmooth2278981112.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanGesture3747783025.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanSnapshot1117222592.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanTouch1022081457.h"
#include "AssemblyU2DCSharp_AnimatorSpeed797755524.h"
#include "AssemblyU2DCSharp_Exit2830455904.h"
#include "AssemblyU2DCSharp_LoadingBar1246465185.h"
#include "AssemblyU2DCSharp_LookAtCamera3167693141.h"
#include "AssemblyU2DCSharp_ReversedGameobject1723523261.h"
#include "AssemblyU2DCSharp_NextsScence2555474679.h"
#include "AssemblyU2DCSharp_PanelButton4067832378.h"
#include "AssemblyU2DCSharp_loadlevel2580852430.h"
#include "AssemblyU2DCSharp_ColourChange1482770568.h"
#include "AssemblyU2DCSharp_PlayVideo2858951647.h"
#include "AssemblyU2DCSharp_PlayVideo_U3CPlayFullscreenVideo1003302819.h"
#include "AssemblyU2DCSharp_TrackableEventHandler3812462983.h"
#include "AssemblyU2DCSharp_VideoPlaybackMenuOptions1188888539.h"
#include "AssemblyU2DCSharp_VideoPlaybackTapHandler4051201625.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour222960481.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour_U3CInitVi1129471401.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour_U3CLoadVi1370610523.h"
#include "AssemblyU2DCSharp_VideoPlaybackBehaviour_U3CPrepar4121586543.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper1808751630.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaState921245858.h"
#include "AssemblyU2DCSharp_VideoPlayerHelper_MediaType3153657181.h"
#include "AssemblyU2DCSharp_Vuforia_BackgroundPlaneBehaviour2431285219.h"
#include "AssemblyU2DCSharp_Vuforia_CloudRecoBehaviour3077176941.h"
#include "AssemblyU2DCSharp_Vuforia_CylinderTargetBehaviour2091399712.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultInitializationErro965510117.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultSmartTerrainEventH870608571.h"
#include "AssemblyU2DCSharp_Vuforia_DefaultTrackableEventHan1082256726.h"
#include "AssemblyU2DCSharp_Vuforia_GLErrorHandler3809113141.h"
#include "AssemblyU2DCSharp_Vuforia_HideExcessAreaBehaviour3495034315.h"
#include "AssemblyU2DCSharp_Vuforia_ImageTargetBehaviour2654589389.h"
#include "AssemblyU2DCSharp_Vuforia_AndroidUnityPlayer852788525.h"
#include "AssemblyU2DCSharp_Vuforia_ComponentFactoryStarterB3249343815.h"
#include "AssemblyU2DCSharp_Vuforia_IOSUnityPlayer3656371703.h"
#include "AssemblyU2DCSharp_Vuforia_VuforiaBehaviourComponen1383853028.h"



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (CameraSettings_t3536359094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2000[4] = 
{
	CameraSettings_t3536359094::get_offset_of_mVuforiaStarted_2(),
	CameraSettings_t3536359094::get_offset_of_mAutofocusEnabled_3(),
	CameraSettings_t3536359094::get_offset_of_mFlashTorchEnabled_4(),
	CameraSettings_t3536359094::get_offset_of_mActiveDirection_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[4] = 
{
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24this_0(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24current_1(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24disposing_2(),
	U3CRestoreOriginalFocusModeU3Ec__Iterator0_t3562025758::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (FrameRateSettings_t1863881954), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (InitErrorHandler_t1791388012), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2003[3] = 
{
	InitErrorHandler_t1791388012::get_offset_of_errorText_2(),
	InitErrorHandler_t1791388012::get_offset_of_errorCanvas_3(),
	InitErrorHandler_t1791388012::get_offset_of_key_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (MenuAnimator_t2049002970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2004[7] = 
{
	MenuAnimator_t2049002970::get_offset_of_mVisiblePos_2(),
	MenuAnimator_t2049002970::get_offset_of_mInvisiblePos_3(),
	MenuAnimator_t2049002970::get_offset_of_mVisibility_4(),
	MenuAnimator_t2049002970::get_offset_of_mVisible_5(),
	MenuAnimator_t2049002970::get_offset_of_mCanvas_6(),
	MenuAnimator_t2049002970::get_offset_of_mMenuOptions_7(),
	MenuAnimator_t2049002970::get_offset_of_SlidingTime_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (MenuOptions_t3210604277), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2005[3] = 
{
	MenuOptions_t3210604277::get_offset_of_mCamSettings_2(),
	MenuOptions_t3210604277::get_offset_of_mTrackableSettings_3(),
	MenuOptions_t3210604277::get_offset_of_mMenuAnim_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (AboutScreen_t3562380015), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (AsyncSceneLoader_t2733707743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2007[1] = 
{
	AsyncSceneLoader_t2733707743::get_offset_of_loadingDelay_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2008[4] = 
{
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_seconds_0(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_U24current_1(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_U24disposing_2(),
	U3CLoadNextSceneAfterU3Ec__Iterator0_t1824579776::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (LoadingScreen_t2880017196), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2009[2] = 
{
	LoadingScreen_t2880017196::get_offset_of_mChangeLevel_2(),
	LoadingScreen_t2880017196::get_offset_of_mUISpinner_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (TapHandler_t3409799063), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2010[4] = 
{
	0,
	TapHandler_t3409799063::get_offset_of_mTimeSinceLastTap_3(),
	TapHandler_t3409799063::get_offset_of_mMenuAnim_4(),
	TapHandler_t3409799063::get_offset_of_mTapCount_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (TrackableSettings_t4265251850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2011[1] = 
{
	TrackableSettings_t4265251850::get_offset_of_mExtTrackingEnabled_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (LeanDestroy_t3536643294), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2012[1] = 
{
	LeanDestroy_t3536643294::get_offset_of_Seconds_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (LeanDragLine_t550158934), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (LeanDragTrail_t3533323662), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2014[3] = 
{
	LeanDragTrail_t3533323662::get_offset_of_Prefab_2(),
	LeanDragTrail_t3533323662::get_offset_of_Distance_3(),
	LeanDragTrail_t3533323662::get_offset_of_links_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (Link_t390936538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	Link_t390936538::get_offset_of_Finger_0(),
	Link_t390936538::get_offset_of_Line_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (LeanFingerDown_t1453848917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2016[2] = 
{
	LeanFingerDown_t1453848917::get_offset_of_IgnoreIfOverGui_2(),
	LeanFingerDown_t1453848917::get_offset_of_OnFingerDown_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (LeanFingerEvent_t4217027252), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (LeanFingerHeld_t681037536), -1, sizeof(LeanFingerHeld_t681037536_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2018[11] = 
{
	LeanFingerHeld_t681037536::get_offset_of_IgnoreIfStartedOverGui_2(),
	LeanFingerHeld_t681037536::get_offset_of_MinimumAge_3(),
	LeanFingerHeld_t681037536::get_offset_of_MaximumMovement_4(),
	LeanFingerHeld_t681037536::get_offset_of_onFingerHeldDown_5(),
	LeanFingerHeld_t681037536::get_offset_of_onFingerHeldSet_6(),
	LeanFingerHeld_t681037536::get_offset_of_onFingerHeldUp_7(),
	LeanFingerHeld_t681037536_StaticFields::get_offset_of_Instances_8(),
	LeanFingerHeld_t681037536_StaticFields::get_offset_of_OnFingerHeldDown_9(),
	LeanFingerHeld_t681037536_StaticFields::get_offset_of_OnFingerHeldSet_10(),
	LeanFingerHeld_t681037536_StaticFields::get_offset_of_OnFingerHeldUp_11(),
	LeanFingerHeld_t681037536::get_offset_of_links_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (FingerEvent_t1929589405), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (Link_t182636800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2020[4] = 
{
	Link_t182636800::get_offset_of_Finger_0(),
	Link_t182636800::get_offset_of_Set_1(),
	Link_t182636800::get_offset_of_LastSet_2(),
	Link_t182636800::get_offset_of_TotalScaledDelta_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (LeanFingerSwipe_t3716445899), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2021[2] = 
{
	LeanFingerSwipe_t3716445899::get_offset_of_IgnoreGuiFingers_2(),
	LeanFingerSwipe_t3716445899::get_offset_of_OnFingerSwipe_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (FingerEvent_t1048019214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (LeanFingerTap_t2593896000), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2023[5] = 
{
	LeanFingerTap_t2593896000::get_offset_of_IgnoreIfOverGui_2(),
	LeanFingerTap_t2593896000::get_offset_of_IgnoreIfStartedOverGui_3(),
	LeanFingerTap_t2593896000::get_offset_of_RequiredTapCount_4(),
	LeanFingerTap_t2593896000::get_offset_of_RequiredTapInterval_5(),
	LeanFingerTap_t2593896000::get_offset_of_OnFingerTap_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (LeanFingerEvent_t1916295887), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (LeanFingerUp_t2387037340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2025[3] = 
{
	LeanFingerUp_t2387037340::get_offset_of_IgnoreIfOverGui_2(),
	LeanFingerUp_t2387037340::get_offset_of_IgnoreIfStartedOverGui_3(),
	LeanFingerUp_t2387037340::get_offset_of_OnFingerUp_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (LeanFingerEvent_t2500729307), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (LeanMultiTap_t2693737624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2027[7] = 
{
	LeanMultiTap_t2693737624::get_offset_of_IgnoreGuiFingers_2(),
	LeanMultiTap_t2693737624::get_offset_of_MultiTap_3(),
	LeanMultiTap_t2693737624::get_offset_of_MultiTapCount_4(),
	LeanMultiTap_t2693737624::get_offset_of_HighestFingerCount_5(),
	LeanMultiTap_t2693737624::get_offset_of_OnMultiTap_6(),
	LeanMultiTap_t2693737624::get_offset_of_age_7(),
	LeanMultiTap_t2693737624::get_offset_of_lastFingerCount_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (IntEvent_t18007207), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (LeanMultiTapInfo_t4075083494), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2029[1] = 
{
	LeanMultiTapInfo_t4075083494::get_offset_of_Text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (LeanOpenUrl_t3701782623), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (LeanPan_t644466001), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2031[4] = 
{
	LeanPan_t644466001::get_offset_of_IgnoreGuiFingers_2(),
	LeanPan_t644466001::get_offset_of_RequiredFingerCount_3(),
	LeanPan_t644466001::get_offset_of_Camera_4(),
	LeanPan_t644466001::get_offset_of_Distance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (LeanPanSmooth_t2363787483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2032[6] = 
{
	LeanPanSmooth_t2363787483::get_offset_of_IgnoreGuiFingers_2(),
	LeanPanSmooth_t2363787483::get_offset_of_RequiredFingerCount_3(),
	LeanPanSmooth_t2363787483::get_offset_of_Camera_4(),
	LeanPanSmooth_t2363787483::get_offset_of_Distance_5(),
	LeanPanSmooth_t2363787483::get_offset_of_Dampening_6(),
	LeanPanSmooth_t2363787483::get_offset_of_RemainingDelta_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (LeanPressSelect_t4275113321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2033[5] = 
{
	LeanPressSelect_t4275113321::get_offset_of_IgnoreGuiFingers_2(),
	LeanPressSelect_t4275113321::get_offset_of_SelectUsing_3(),
	LeanPressSelect_t4275113321::get_offset_of_LayerMask_4(),
	LeanPressSelect_t4275113321::get_offset_of_Search_5(),
	LeanPressSelect_t4275113321::get_offset_of_CurrentSelectables_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (SelectType_t1810901247)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2034[3] = 
{
	SelectType_t1810901247::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (SearchType_t3326327913)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2035[4] = 
{
	SearchType_t3326327913::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (LeanRotate_t827445339), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2036[6] = 
{
	LeanRotate_t827445339::get_offset_of_IgnoreGuiFingers_2(),
	LeanRotate_t827445339::get_offset_of_RequiredFingerCount_3(),
	LeanRotate_t827445339::get_offset_of_RequiredSelectable_4(),
	LeanRotate_t827445339::get_offset_of_Camera_5(),
	LeanRotate_t827445339::get_offset_of_RotateAxis_6(),
	LeanRotate_t827445339::get_offset_of_Relative_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (LeanScale_t2479469360), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2037[6] = 
{
	LeanScale_t2479469360::get_offset_of_IgnoreGuiFingers_2(),
	LeanScale_t2479469360::get_offset_of_RequiredFingerCount_3(),
	LeanScale_t2479469360::get_offset_of_RequiredSelectable_4(),
	LeanScale_t2479469360::get_offset_of_WheelSensitivity_5(),
	LeanScale_t2479469360::get_offset_of_Camera_6(),
	LeanScale_t2479469360::get_offset_of_Relative_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (LeanSelect_t3606489640), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2038[6] = 
{
	LeanSelect_t3606489640::get_offset_of_SelectUsing_2(),
	LeanSelect_t3606489640::get_offset_of_LayerMask_3(),
	LeanSelect_t3606489640::get_offset_of_Search_4(),
	LeanSelect_t3606489640::get_offset_of_CurrentSelectable_5(),
	LeanSelect_t3606489640::get_offset_of_Reselect_6(),
	LeanSelect_t3606489640::get_offset_of_AutoDeselect_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (SelectType_t2074988034)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2039[3] = 
{
	SelectType_t2074988034::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (SearchType_t3831610778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2040[4] = 
{
	SearchType_t3831610778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (ReselectType_t399732767)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2041[5] = 
{
	ReselectType_t399732767::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (LeanSelectable_t3692576450), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2042[5] = 
{
	LeanSelectable_t3692576450::get_offset_of_IsSelected_2(),
	LeanSelectable_t3692576450::get_offset_of_SelectingFinger_3(),
	LeanSelectable_t3692576450::get_offset_of_OnSelect_4(),
	LeanSelectable_t3692576450::get_offset_of_OnSelectUp_5(),
	LeanSelectable_t3692576450::get_offset_of_OnDeselect_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (LeanFingerEvent_t2546124889), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (LeanSelectableBehaviour_t2782920709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2044[1] = 
{
	LeanSelectableBehaviour_t2782920709::get_offset_of_selectable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (LeanSelectableRendererColor_t652725566), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2045[3] = 
{
	LeanSelectableRendererColor_t652725566::get_offset_of_AutoGetDefaultColor_3(),
	LeanSelectableRendererColor_t652725566::get_offset_of_DefaultColor_4(),
	LeanSelectableRendererColor_t652725566::get_offset_of_SelectedColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (LeanSelectableSpriteRendererColor_t1641095051), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2046[3] = 
{
	LeanSelectableSpriteRendererColor_t1641095051::get_offset_of_AutoGetDefaultColor_3(),
	LeanSelectableSpriteRendererColor_t1641095051::get_offset_of_DefaultColor_4(),
	LeanSelectableSpriteRendererColor_t1641095051::get_offset_of_SelectedColor_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (LeanSelectableTranslateInertia2D_t2273971066), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (LeanSelectableTranslateInertia3D_t3840055007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2048[1] = 
{
	LeanSelectableTranslateInertia3D_t3840055007::get_offset_of_body_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (LeanSpawnGameObject_t1465354280), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2049[2] = 
{
	LeanSpawnGameObject_t1465354280::get_offset_of_Prefab_2(),
	LeanSpawnGameObject_t1465354280::get_offset_of_FingerDistance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (LeanSwipeDirection4_t3774896743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2050[2] = 
{
	LeanSwipeDirection4_t3774896743::get_offset_of_InfoText_2(),
	LeanSwipeDirection4_t3774896743::get_offset_of_anim_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (LeanSwipeDirection8_t1449297915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2051[2] = 
{
	LeanSwipeDirection8_t1449297915::get_offset_of_InfoText_2(),
	LeanSwipeDirection8_t1449297915::get_offset_of_Video_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (LeanSwipeRigidbody2D_t1364749039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2052[2] = 
{
	LeanSwipeRigidbody2D_t1364749039::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody2D_t1364749039::get_offset_of_ForceMultiplier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (LeanSwipeRigidbody2DNoRelease_t2933744255), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2053[3] = 
{
	LeanSwipeRigidbody2DNoRelease_t2933744255::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody2DNoRelease_t2933744255::get_offset_of_ImpulseForce_3(),
	LeanSwipeRigidbody2DNoRelease_t2933744255::get_offset_of_swipingFinger_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (LeanSwipeRigidbody3D_t4093632394), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2054[2] = 
{
	LeanSwipeRigidbody3D_t4093632394::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody3D_t4093632394::get_offset_of_ForceMultiplier_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (LeanSwipeRigidbody3DNoRelease_t3590775226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2055[3] = 
{
	LeanSwipeRigidbody3DNoRelease_t3590775226::get_offset_of_LayerMask_2(),
	LeanSwipeRigidbody3DNoRelease_t3590775226::get_offset_of_ImpulseForce_3(),
	LeanSwipeRigidbody3DNoRelease_t3590775226::get_offset_of_swipingFinger_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (LeanTapSelect_t4283262707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2056[3] = 
{
	LeanTapSelect_t4283262707::get_offset_of_IgnoreGuiFingers_8(),
	LeanTapSelect_t4283262707::get_offset_of_RequiredTapCount_9(),
	LeanTapSelect_t4283262707::get_offset_of_RequiredTapInterval_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (LeanTouchEvents_t41969022), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (LeanTranslate_t3292440434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2058[4] = 
{
	LeanTranslate_t3292440434::get_offset_of_IgnoreGuiFingers_2(),
	LeanTranslate_t3292440434::get_offset_of_RequiredFingerCount_3(),
	LeanTranslate_t3292440434::get_offset_of_RequiredSelectable_4(),
	LeanTranslate_t3292440434::get_offset_of_Camera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (LeanTranslateSmooth_t748462924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2059[6] = 
{
	LeanTranslateSmooth_t748462924::get_offset_of_IgnoreGuiFingers_2(),
	LeanTranslateSmooth_t748462924::get_offset_of_RequiredFingerCount_3(),
	LeanTranslateSmooth_t748462924::get_offset_of_RequiredSelectable_4(),
	LeanTranslateSmooth_t748462924::get_offset_of_Camera_5(),
	LeanTranslateSmooth_t748462924::get_offset_of_Smoothness_6(),
	LeanTranslateSmooth_t748462924::get_offset_of_RemainingDelta_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (LeanZoomCamera_t246347926), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2060[6] = 
{
	LeanZoomCamera_t246347926::get_offset_of_IgnoreGuiFingers_2(),
	LeanZoomCamera_t246347926::get_offset_of_RequiredFingerCount_3(),
	LeanZoomCamera_t246347926::get_offset_of_WheelSensitivity_4(),
	LeanZoomCamera_t246347926::get_offset_of_Camera_5(),
	LeanZoomCamera_t246347926::get_offset_of_Minimum_6(),
	LeanZoomCamera_t246347926::get_offset_of_Maximum_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (LeanZoomCameraSmooth_t2278981112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2061[8] = 
{
	LeanZoomCameraSmooth_t2278981112::get_offset_of_IgnoreGuiFingers_2(),
	LeanZoomCameraSmooth_t2278981112::get_offset_of_RequiredFingerCount_3(),
	LeanZoomCameraSmooth_t2278981112::get_offset_of_WheelSensitivity_4(),
	LeanZoomCameraSmooth_t2278981112::get_offset_of_Camera_5(),
	LeanZoomCameraSmooth_t2278981112::get_offset_of_Target_6(),
	LeanZoomCameraSmooth_t2278981112::get_offset_of_Minimum_7(),
	LeanZoomCameraSmooth_t2278981112::get_offset_of_Maximum_8(),
	LeanZoomCameraSmooth_t2278981112::get_offset_of_Dampening_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (LeanFinger_t76062517), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2062[12] = 
{
	LeanFinger_t76062517::get_offset_of_Index_0(),
	LeanFinger_t76062517::get_offset_of_Age_1(),
	LeanFinger_t76062517::get_offset_of_Set_2(),
	LeanFinger_t76062517::get_offset_of_LastSet_3(),
	LeanFinger_t76062517::get_offset_of_Tap_4(),
	LeanFinger_t76062517::get_offset_of_TapCount_5(),
	LeanFinger_t76062517::get_offset_of_Swipe_6(),
	LeanFinger_t76062517::get_offset_of_StartScreenPosition_7(),
	LeanFinger_t76062517::get_offset_of_LastScreenPosition_8(),
	LeanFinger_t76062517::get_offset_of_ScreenPosition_9(),
	LeanFinger_t76062517::get_offset_of_StartedOverGui_10(),
	LeanFinger_t76062517::get_offset_of_Snapshots_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (LeanGesture_t3747783025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { sizeof (LeanSnapshot_t1117222592), -1, sizeof(LeanSnapshot_t1117222592_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2064[3] = 
{
	LeanSnapshot_t1117222592::get_offset_of_Age_0(),
	LeanSnapshot_t1117222592::get_offset_of_ScreenPosition_1(),
	LeanSnapshot_t1117222592_StaticFields::get_offset_of_InactiveSnapshots_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (LeanTouch_t1022081457), -1, sizeof(LeanTouch_t1022081457_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2065[25] = 
{
	LeanTouch_t1022081457_StaticFields::get_offset_of_Instances_2(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_Fingers_3(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_InactiveFingers_4(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_OnFingerDown_5(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_OnFingerSet_6(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_OnFingerUp_7(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_OnFingerTap_8(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_OnFingerSwipe_9(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_OnGesture_10(),
	LeanTouch_t1022081457::get_offset_of_TapThreshold_11(),
	LeanTouch_t1022081457::get_offset_of_SwipeThreshold_12(),
	LeanTouch_t1022081457::get_offset_of_ReferenceDpi_13(),
	LeanTouch_t1022081457::get_offset_of_GuiLayers_14(),
	LeanTouch_t1022081457::get_offset_of_RecordFingers_15(),
	LeanTouch_t1022081457::get_offset_of_RecordThreshold_16(),
	LeanTouch_t1022081457::get_offset_of_RecordLimit_17(),
	LeanTouch_t1022081457::get_offset_of_SimulateMultiFingers_18(),
	LeanTouch_t1022081457::get_offset_of_PinchTwistKey_19(),
	LeanTouch_t1022081457::get_offset_of_MultiDragKey_20(),
	LeanTouch_t1022081457::get_offset_of_FingerTexture_21(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_highestMouseButton_22(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_tempRaycastResults_23(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_filteredFingers_24(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_tempPointerEventData_25(),
	LeanTouch_t1022081457_StaticFields::get_offset_of_tempEventSystem_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (AnimatorSpeed_t797755524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2066[1] = 
{
	AnimatorSpeed_t797755524::get_offset_of_anim_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (Exit_t2830455904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (LoadingBar_t1246465185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[3] = 
{
	LoadingBar_t1246465185::get_offset_of_ao_2(),
	LoadingBar_t1246465185::get_offset_of_LoadingImage_3(),
	LoadingBar_t1246465185::get_offset_of_LoadingText_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (LookAtCamera_t3167693141), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (ReversedGameobject_t1723523261), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2070[1] = 
{
	ReversedGameobject_t1723523261::get_offset_of_Text_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (NextsScence_t2555474679), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (PanelButton_t4067832378), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (loadlevel_t2580852430), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (ColourChange_t1482770568), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2074[3] = 
{
	ColourChange_t1482770568::get_offset_of_text1_2(),
	ColourChange_t1482770568::get_offset_of_text2_3(),
	ColourChange_t1482770568::get_offset_of_text3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (PlayVideo_t2858951647), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2075[2] = 
{
	PlayVideo_t2858951647::get_offset_of_playFullscreen_2(),
	PlayVideo_t2858951647::get_offset_of_currentVideo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2076[4] = 
{
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_video_0(),
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_U24current_1(),
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_U24disposing_2(),
	U3CPlayFullscreenVideoAtEndOfFrameU3Ec__Iterator0_t1003302819::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (TrackableEventHandler_t3812462983), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2077[5] = 
{
	TrackableEventHandler_t3812462983::get_offset_of_mTrackableBehaviour_2(),
	TrackableEventHandler_t3812462983::get_offset_of_mHasBeenFound_3(),
	TrackableEventHandler_t3812462983::get_offset_of_mLostTracking_4(),
	TrackableEventHandler_t3812462983::get_offset_of_mSecondsSinceLost_5(),
	TrackableEventHandler_t3812462983::get_offset_of_Butto_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (VideoPlaybackMenuOptions_t1188888539), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[1] = 
{
	VideoPlaybackMenuOptions_t1188888539::get_offset_of_mPlayVideo_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (VideoPlaybackTapHandler_t4051201625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[1] = 
{
	VideoPlaybackTapHandler_t4051201625::get_offset_of_mPlayVideo_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (VideoPlaybackBehaviour_t222960481), -1, sizeof(VideoPlaybackBehaviour_t222960481_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2080[18] = 
{
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_path_2(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_playTexture_3(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_busyTexture_4(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_errorTexture_5(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_m_autoPlay_6(),
	VideoPlaybackBehaviour_t222960481_StaticFields::get_offset_of_sLoadingLocked_7(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mVideoPlayer_8(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mIsInited_9(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mInitInProgess_10(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mAppPaused_11(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mVideoTexture_12(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mKeyframeTexture_13(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mMediaType_14(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mCurrentState_15(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mSeekPosition_16(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_isPlayableOnTexture_17(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mIconPlane_18(),
	VideoPlaybackBehaviour_t222960481::get_offset_of_mIconPlaneActive_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (U3CInitVideoPlayerU3Ec__Iterator0_t1129471401), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[5] = 
{
	U3CInitVideoPlayerU3Ec__Iterator0_t1129471401::get_offset_of_U3CrendererAPIU3E__0_0(),
	U3CInitVideoPlayerU3Ec__Iterator0_t1129471401::get_offset_of_U24this_1(),
	U3CInitVideoPlayerU3Ec__Iterator0_t1129471401::get_offset_of_U24current_2(),
	U3CInitVideoPlayerU3Ec__Iterator0_t1129471401::get_offset_of_U24disposing_3(),
	U3CInitVideoPlayerU3Ec__Iterator0_t1129471401::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (U3CLoadVideoU3Ec__Iterator1_t1370610523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2082[4] = 
{
	U3CLoadVideoU3Ec__Iterator1_t1370610523::get_offset_of_U24this_0(),
	U3CLoadVideoU3Ec__Iterator1_t1370610523::get_offset_of_U24current_1(),
	U3CLoadVideoU3Ec__Iterator1_t1370610523::get_offset_of_U24disposing_2(),
	U3CLoadVideoU3Ec__Iterator1_t1370610523::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (U3CPrepareVideoU3Ec__Iterator2_t4121586543), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2083[6] = 
{
	U3CPrepareVideoU3Ec__Iterator2_t4121586543::get_offset_of_U3CstateU3E__0_0(),
	U3CPrepareVideoU3Ec__Iterator2_t4121586543::get_offset_of_U3CisOpenGLRenderingU3E__1_1(),
	U3CPrepareVideoU3Ec__Iterator2_t4121586543::get_offset_of_U24this_2(),
	U3CPrepareVideoU3Ec__Iterator2_t4121586543::get_offset_of_U24current_3(),
	U3CPrepareVideoU3Ec__Iterator2_t4121586543::get_offset_of_U24disposing_4(),
	U3CPrepareVideoU3Ec__Iterator2_t4121586543::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (VideoPlayerHelper_t1808751630), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[3] = 
{
	VideoPlayerHelper_t1808751630::get_offset_of_mFilename_0(),
	VideoPlayerHelper_t1808751630::get_offset_of_mFullScreenFilename_1(),
	VideoPlayerHelper_t1808751630::get_offset_of_mVideoPlayerPtr_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (MediaState_t921245858)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[9] = 
{
	MediaState_t921245858::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (MediaType_t3153657181)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2086[4] = 
{
	MediaType_t3153657181::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (BackgroundPlaneBehaviour_t2431285219), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (CloudRecoBehaviour_t3077176941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (CylinderTargetBehaviour_t2091399712), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (DefaultInitializationErrorHandler_t965510117), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[3] = 
{
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorText_2(),
	DefaultInitializationErrorHandler_t965510117::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (DefaultSmartTerrainEventHandler_t870608571), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[3] = 
{
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_mReconstructionBehaviour_2(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_PropTemplate_3(),
	DefaultSmartTerrainEventHandler_t870608571::get_offset_of_SurfaceTemplate_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (DefaultTrackableEventHandler_t1082256726), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2092[1] = 
{
	DefaultTrackableEventHandler_t1082256726::get_offset_of_mTrackableBehaviour_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (GLErrorHandler_t3809113141), -1, sizeof(GLErrorHandler_t3809113141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2093[3] = 
{
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorText_2(),
	GLErrorHandler_t3809113141_StaticFields::get_offset_of_mErrorOccurred_3(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (HideExcessAreaBehaviour_t3495034315), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (ImageTargetBehaviour_t2654589389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (AndroidUnityPlayer_t852788525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[6] = 
{
	0,
	0,
	AndroidUnityPlayer_t852788525::get_offset_of_mScreenOrientation_2(),
	AndroidUnityPlayer_t852788525::get_offset_of_mJavaScreenOrientation_3(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastOrientationReset_4(),
	AndroidUnityPlayer_t852788525::get_offset_of_mFramesSinceLastJavaOrientationCheck_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (ComponentFactoryStarterBehaviour_t3249343815), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (IOSUnityPlayer_t3656371703), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[1] = 
{
	IOSUnityPlayer_t3656371703::get_offset_of_mScreenOrientation_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (VuforiaBehaviourComponentFactory_t1383853028), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
