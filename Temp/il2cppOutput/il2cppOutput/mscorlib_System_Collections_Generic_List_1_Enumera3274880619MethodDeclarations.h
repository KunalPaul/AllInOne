﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanFinger>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2236561571(__this, ___l0, method) ((  void (*) (Enumerator_t3274880619 *, List_1_t3740150945 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanFinger>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2246744687(__this, method) ((  void (*) (Enumerator_t3274880619 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanFinger>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m992349059(__this, method) ((  Il2CppObject * (*) (Enumerator_t3274880619 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanFinger>::Dispose()
#define Enumerator_Dispose_m651909664(__this, method) ((  void (*) (Enumerator_t3274880619 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanFinger>::VerifyState()
#define Enumerator_VerifyState_m1368501233(__this, method) ((  void (*) (Enumerator_t3274880619 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanFinger>::MoveNext()
#define Enumerator_MoveNext_m3303420347(__this, method) ((  bool (*) (Enumerator_t3274880619 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanFinger>::get_Current()
#define Enumerator_get_Current_m912179678(__this, method) ((  LeanFinger_t76062517 * (*) (Enumerator_t3274880619 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
