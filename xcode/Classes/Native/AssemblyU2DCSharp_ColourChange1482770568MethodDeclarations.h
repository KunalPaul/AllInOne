﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ColourChange
struct ColourChange_t1482770568;

#include "codegen/il2cpp-codegen.h"

// System.Void ColourChange::.ctor()
extern "C"  void ColourChange__ctor_m331451617 (ColourChange_t1482770568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColourChange::Start()
extern "C"  void ColourChange_Start_m123327977 (ColourChange_t1482770568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ColourChange::Update()
extern "C"  void ColourChange_Update_m1796815986 (ColourChange_t1482770568 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
