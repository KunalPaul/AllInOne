﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanDragLine
struct LeanDragLine_t550158934;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanDragLine::.ctor()
extern "C"  void LeanDragLine__ctor_m3981925724 (LeanDragLine_t550158934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanDragLine::WritePositions(UnityEngine.LineRenderer,Lean.Touch.LeanFinger)
extern "C"  void LeanDragLine_WritePositions_m1804237179 (LeanDragLine_t550158934 * __this, LineRenderer_t849157671 * ___line0, LeanFinger_t76062517 * ___finger1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
