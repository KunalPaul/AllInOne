﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanDragTrail/Link
struct Link_t390936538;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanDragTrail/Link::.ctor()
extern "C"  void Link__ctor_m3796844163 (Link_t390936538 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
