﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerDown/LeanFingerEvent
struct LeanFingerEvent_t4217027252;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanFingerDown/LeanFingerEvent::.ctor()
extern "C"  void LeanFingerEvent__ctor_m1767046313 (LeanFingerEvent_t4217027252 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
