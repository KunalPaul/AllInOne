﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerHeld/Link
struct Link_t182636800;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanFingerHeld/Link::.ctor()
extern "C"  void Link__ctor_m3897746909 (Link_t182636800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
