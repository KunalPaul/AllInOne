﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerSwipe
struct LeanFingerSwipe_t3716445899;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanFingerSwipe::.ctor()
extern "C"  void LeanFingerSwipe__ctor_m3216412971 (LeanFingerSwipe_t3716445899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerSwipe::OnEnable()
extern "C"  void LeanFingerSwipe_OnEnable_m658902039 (LeanFingerSwipe_t3716445899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerSwipe::OnDisable()
extern "C"  void LeanFingerSwipe_OnDisable_m2763739482 (LeanFingerSwipe_t3716445899 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerSwipe::OnFingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanFingerSwipe_OnFingerUp_m1301028398 (LeanFingerSwipe_t3716445899 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
