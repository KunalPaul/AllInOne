﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerTap/LeanFingerEvent
struct LeanFingerEvent_t1916295887;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanFingerTap/LeanFingerEvent::.ctor()
extern "C"  void LeanFingerEvent__ctor_m3611773752 (LeanFingerEvent_t1916295887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
