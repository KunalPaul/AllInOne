﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanFingerUp
struct LeanFingerUp_t2387037340;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanFingerUp::.ctor()
extern "C"  void LeanFingerUp__ctor_m3023536452 (LeanFingerUp_t2387037340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerUp::OnEnable()
extern "C"  void LeanFingerUp_OnEnable_m2053233528 (LeanFingerUp_t2387037340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerUp::OnDisable()
extern "C"  void LeanFingerUp_OnDisable_m970954135 (LeanFingerUp_t2387037340 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanFingerUp::FingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanFingerUp_FingerUp_m1107409808 (LeanFingerUp_t2387037340 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
