﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanPanSmooth
struct LeanPanSmooth_t2363787483;

#include "codegen/il2cpp-codegen.h"

// System.Void Lean.Touch.LeanPanSmooth::.ctor()
extern "C"  void LeanPanSmooth__ctor_m2262025975 (LeanPanSmooth_t2363787483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanPanSmooth::LateUpdate()
extern "C"  void LeanPanSmooth_LateUpdate_m1280670356 (LeanPanSmooth_t2363787483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
