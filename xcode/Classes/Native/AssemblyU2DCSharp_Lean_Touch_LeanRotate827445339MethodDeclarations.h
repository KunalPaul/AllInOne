﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanRotate
struct LeanRotate_t827445339;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"

// System.Void Lean.Touch.LeanRotate::.ctor()
extern "C"  void LeanRotate__ctor_m2255888439 (LeanRotate_t827445339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanRotate::Update()
extern "C"  void LeanRotate_Update_m1718295668 (LeanRotate_t827445339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanRotate::Rotate(UnityEngine.Vector3,System.Single)
extern "C"  void LeanRotate_Rotate_m3877790132 (LeanRotate_t827445339 * __this, Vector3_t2243707580  ___center0, float ___degrees1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
