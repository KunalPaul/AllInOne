﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanScale
struct LeanScale_t2479469360;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Lean.Touch.LeanScale::.ctor()
extern "C"  void LeanScale__ctor_m2245606104 (LeanScale_t2479469360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanScale::Update()
extern "C"  void LeanScale_Update_m3741109951 (LeanScale_t2479469360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanScale::Scale(System.Single,UnityEngine.Vector2)
extern "C"  void LeanScale_Scale_m4139219539 (LeanScale_t2479469360 * __this, float ___scale0, Vector2_t2243707579  ___screenCenter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
