﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSelectable
struct LeanSelectable_t3692576450;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSelectable::.ctor()
extern "C"  void LeanSelectable__ctor_m606954148 (LeanSelectable_t3692576450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectable::Select()
extern "C"  void LeanSelectable_Select_m1397276638 (LeanSelectable_t3692576450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectable::Select(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectable_Select_m3668728632 (LeanSelectable_t3692576450 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectable::Deselect()
extern "C"  void LeanSelectable_Deselect_m3746043981 (LeanSelectable_t3692576450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectable::OnEnable()
extern "C"  void LeanSelectable_OnEnable_m1764272852 (LeanSelectable_t3692576450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectable::OnDisable()
extern "C"  void LeanSelectable_OnDisable_m2139115829 (LeanSelectable_t3692576450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectable::OnFingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectable_OnFingerUp_m3291500821 (LeanSelectable_t3692576450 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
