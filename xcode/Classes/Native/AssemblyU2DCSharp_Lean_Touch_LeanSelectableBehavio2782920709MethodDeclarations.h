﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSelectableBehaviour
struct LeanSelectableBehaviour_t2782920709;
// Lean.Touch.LeanSelectable
struct LeanSelectable_t3692576450;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSelectableBehaviour::.ctor()
extern "C"  void LeanSelectableBehaviour__ctor_m3141209765 (LeanSelectableBehaviour_t2782920709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Lean.Touch.LeanSelectable Lean.Touch.LeanSelectableBehaviour::get_Selectable()
extern "C"  LeanSelectable_t3692576450 * LeanSelectableBehaviour_get_Selectable_m3381086028 (LeanSelectableBehaviour_t2782920709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableBehaviour::OnEnable()
extern "C"  void LeanSelectableBehaviour_OnEnable_m2406361789 (LeanSelectableBehaviour_t2782920709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableBehaviour::OnDisable()
extern "C"  void LeanSelectableBehaviour_OnDisable_m645580512 (LeanSelectableBehaviour_t2782920709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableBehaviour::OnSelect(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectableBehaviour_OnSelect_m4254658844 (LeanSelectableBehaviour_t2782920709 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableBehaviour::OnSelectUp(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectableBehaviour_OnSelectUp_m806350785 (LeanSelectableBehaviour_t2782920709 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableBehaviour::OnDeselect()
extern "C"  void LeanSelectableBehaviour_OnDeselect_m17598933 (LeanSelectableBehaviour_t2782920709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
