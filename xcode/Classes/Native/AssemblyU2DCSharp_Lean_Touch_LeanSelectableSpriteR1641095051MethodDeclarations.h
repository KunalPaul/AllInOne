﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSelectableSpriteRendererColor
struct LeanSelectableSpriteRendererColor_t1641095051;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

// System.Void Lean.Touch.LeanSelectableSpriteRendererColor::.ctor()
extern "C"  void LeanSelectableSpriteRendererColor__ctor_m3850989457 (LeanSelectableSpriteRendererColor_t1641095051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableSpriteRendererColor::Awake()
extern "C"  void LeanSelectableSpriteRendererColor_Awake_m1963602230 (LeanSelectableSpriteRendererColor_t1641095051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnSelect(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectableSpriteRendererColor_OnSelect_m2865695818 (LeanSelectableSpriteRendererColor_t1641095051 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableSpriteRendererColor::OnDeselect()
extern "C"  void LeanSelectableSpriteRendererColor_OnDeselect_m2691963129 (LeanSelectableSpriteRendererColor_t1641095051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableSpriteRendererColor::ChangeColor(UnityEngine.Color)
extern "C"  void LeanSelectableSpriteRendererColor_ChangeColor_m3118169162 (LeanSelectableSpriteRendererColor_t1641095051 * __this, Color_t2020392075  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
