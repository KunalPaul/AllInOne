﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSelectableTranslateInertia3D
struct LeanSelectableTranslateInertia3D_t3840055007;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSelectableTranslateInertia3D::.ctor()
extern "C"  void LeanSelectableTranslateInertia3D__ctor_m3113879977 (LeanSelectableTranslateInertia3D_t3840055007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableTranslateInertia3D::Update()
extern "C"  void LeanSelectableTranslateInertia3D_Update_m3529006764 (LeanSelectableTranslateInertia3D_t3840055007 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSelectableTranslateInertia3D::OnSelectUp(Lean.Touch.LeanFinger)
extern "C"  void LeanSelectableTranslateInertia3D_OnSelectUp_m1287157445 (LeanSelectableTranslateInertia3D_t3840055007 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
