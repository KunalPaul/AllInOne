﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSpawnGameObject
struct LeanSpawnGameObject_t1465354280;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSpawnGameObject::.ctor()
extern "C"  void LeanSpawnGameObject__ctor_m52419608 (LeanSpawnGameObject_t1465354280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSpawnGameObject::Spawn()
extern "C"  void LeanSpawnGameObject_Spawn_m1515758185 (LeanSpawnGameObject_t1465354280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSpawnGameObject::Spawn(Lean.Touch.LeanFinger)
extern "C"  void LeanSpawnGameObject_Spawn_m1388379469 (LeanSpawnGameObject_t1465354280 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
