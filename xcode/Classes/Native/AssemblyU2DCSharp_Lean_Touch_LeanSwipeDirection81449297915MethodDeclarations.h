﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSwipeDirection8
struct LeanSwipeDirection8_t1449297915;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Lean.Touch.LeanSwipeDirection8::.ctor()
extern "C"  void LeanSwipeDirection8__ctor_m1957242639 (LeanSwipeDirection8_t1449297915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection8::Start()
extern "C"  void LeanSwipeDirection8_Start_m4089564243 (LeanSwipeDirection8_t1449297915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection8::OnEnable()
extern "C"  void LeanSwipeDirection8_OnEnable_m1812364595 (LeanSwipeDirection8_t1449297915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection8::OnDisable()
extern "C"  void LeanSwipeDirection8_OnDisable_m2563608486 (LeanSwipeDirection8_t1449297915 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeDirection8::OnFingerSwipe(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeDirection8_OnFingerSwipe_m8278047 (LeanSwipeDirection8_t1449297915 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Lean.Touch.LeanSwipeDirection8::SwipedInThisDirection(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool LeanSwipeDirection8_SwipedInThisDirection_m911566015 (LeanSwipeDirection8_t1449297915 * __this, Vector2_t2243707579  ___swipe0, Vector2_t2243707579  ___direction1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
