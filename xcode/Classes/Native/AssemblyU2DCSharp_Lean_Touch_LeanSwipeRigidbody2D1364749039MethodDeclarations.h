﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSwipeRigidbody2D
struct LeanSwipeRigidbody2D_t1364749039;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSwipeRigidbody2D::.ctor()
extern "C"  void LeanSwipeRigidbody2D__ctor_m1394490781 (LeanSwipeRigidbody2D_t1364749039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2D::OnEnable()
extern "C"  void LeanSwipeRigidbody2D_OnEnable_m3311944725 (LeanSwipeRigidbody2D_t1364749039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2D::OnDisable()
extern "C"  void LeanSwipeRigidbody2D_OnDisable_m3441069678 (LeanSwipeRigidbody2D_t1364749039 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody2D::OnFingerSwipe(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody2D_OnFingerSwipe_m3201215761 (LeanSwipeRigidbody2D_t1364749039 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
