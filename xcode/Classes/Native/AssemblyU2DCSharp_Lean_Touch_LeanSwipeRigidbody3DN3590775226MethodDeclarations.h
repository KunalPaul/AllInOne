﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanSwipeRigidbody3DNoRelease
struct LeanSwipeRigidbody3DNoRelease_t3590775226;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanSwipeRigidbody3DNoRelease::.ctor()
extern "C"  void LeanSwipeRigidbody3DNoRelease__ctor_m1119983940 (LeanSwipeRigidbody3DNoRelease_t3590775226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3DNoRelease::OnEnable()
extern "C"  void LeanSwipeRigidbody3DNoRelease_OnEnable_m3654001348 (LeanSwipeRigidbody3DNoRelease_t3590775226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3DNoRelease::OnDisable()
extern "C"  void LeanSwipeRigidbody3DNoRelease_OnDisable_m1797789649 (LeanSwipeRigidbody3DNoRelease_t3590775226 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3DNoRelease::OnFingerSet(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody3DNoRelease_OnFingerSet_m2893691846 (LeanSwipeRigidbody3DNoRelease_t3590775226 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3DNoRelease::OnFingerDown(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody3DNoRelease_OnFingerDown_m1684921532 (LeanSwipeRigidbody3DNoRelease_t3590775226 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanSwipeRigidbody3DNoRelease::OnFingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanSwipeRigidbody3DNoRelease_OnFingerUp_m4261491497 (LeanSwipeRigidbody3DNoRelease_t3590775226 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
