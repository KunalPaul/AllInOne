﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanTapSelect
struct LeanTapSelect_t4283262707;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanTapSelect::.ctor()
extern "C"  void LeanTapSelect__ctor_m179932759 (LeanTapSelect_t4283262707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTapSelect::OnEnable()
extern "C"  void LeanTapSelect_OnEnable_m217991787 (LeanTapSelect_t4283262707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTapSelect::OnDisable()
extern "C"  void LeanTapSelect_OnDisable_m3085279854 (LeanTapSelect_t4283262707 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTapSelect::FingerTap(Lean.Touch.LeanFinger)
extern "C"  void LeanTapSelect_FingerTap_m1643018411 (LeanTapSelect_t4283262707 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
