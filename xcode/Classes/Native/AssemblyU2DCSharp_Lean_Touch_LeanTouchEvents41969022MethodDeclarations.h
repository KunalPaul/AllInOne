﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanTouchEvents
struct LeanTouchEvents_t41969022;
// Lean.Touch.LeanFinger
struct LeanFinger_t76062517;
// System.Collections.Generic.List`1<Lean.Touch.LeanFinger>
struct List_1_t3740150945;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Lean_Touch_LeanFinger76062517.h"

// System.Void Lean.Touch.LeanTouchEvents::.ctor()
extern "C"  void LeanTouchEvents__ctor_m2636858936 (LeanTouchEvents_t41969022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnEnable()
extern "C"  void LeanTouchEvents_OnEnable_m657859528 (LeanTouchEvents_t41969022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnDisable()
extern "C"  void LeanTouchEvents_OnDisable_m2060800237 (LeanTouchEvents_t41969022 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnFingerDown(Lean.Touch.LeanFinger)
extern "C"  void LeanTouchEvents_OnFingerDown_m2651983216 (LeanTouchEvents_t41969022 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnFingerSet(Lean.Touch.LeanFinger)
extern "C"  void LeanTouchEvents_OnFingerSet_m232772058 (LeanTouchEvents_t41969022 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnFingerUp(Lean.Touch.LeanFinger)
extern "C"  void LeanTouchEvents_OnFingerUp_m3718302333 (LeanTouchEvents_t41969022 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnFingerTap(Lean.Touch.LeanFinger)
extern "C"  void LeanTouchEvents_OnFingerTap_m411162413 (LeanTouchEvents_t41969022 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnFingerSwipe(Lean.Touch.LeanFinger)
extern "C"  void LeanTouchEvents_OnFingerSwipe_m2016335660 (LeanTouchEvents_t41969022 * __this, LeanFinger_t76062517 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTouchEvents::OnGesture(System.Collections.Generic.List`1<Lean.Touch.LeanFinger>)
extern "C"  void LeanTouchEvents_OnGesture_m2462510520 (LeanTouchEvents_t41969022 * __this, List_1_t3740150945 * ___fingers0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
