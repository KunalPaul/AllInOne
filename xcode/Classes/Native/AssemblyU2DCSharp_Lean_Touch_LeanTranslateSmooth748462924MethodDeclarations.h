﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Lean.Touch.LeanTranslateSmooth
struct LeanTranslateSmooth_t748462924;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

// System.Void Lean.Touch.LeanTranslateSmooth::.ctor()
extern "C"  void LeanTranslateSmooth__ctor_m2964573214 (LeanTranslateSmooth_t748462924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTranslateSmooth::Update()
extern "C"  void LeanTranslateSmooth_Update_m2295540031 (LeanTranslateSmooth_t748462924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTranslateSmooth::LateUpdate()
extern "C"  void LeanTranslateSmooth_LateUpdate_m2420448171 (LeanTranslateSmooth_t748462924 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Lean.Touch.LeanTranslateSmooth::Translate(UnityEngine.Vector2)
extern "C"  void LeanTranslateSmooth_Translate_m1624748624 (LeanTranslateSmooth_t748462924 * __this, Vector2_t2243707579  ___screenDelta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
