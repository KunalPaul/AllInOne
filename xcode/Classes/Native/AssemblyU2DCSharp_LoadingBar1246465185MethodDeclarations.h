﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingBar
struct LoadingBar_t1246465185;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadingBar::.ctor()
extern "C"  void LoadingBar__ctor_m86632608 (LoadingBar_t1246465185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBar::Start()
extern "C"  void LoadingBar_Start_m2396812640 (LoadingBar_t1246465185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBar::Update()
extern "C"  void LoadingBar_Update_m3209067625 (LoadingBar_t1246465185 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
