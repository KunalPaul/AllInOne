﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen2727799310MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::.ctor()
#define UnityEvent_1__ctor_m1090789293(__this, method) ((  void (*) (UnityEvent_1_t114412532 *, const MethodInfo*))UnityEvent_1__ctor_m2073978020_gshared)(__this, method)
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_AddListener_m3094341323(__this, ___call0, method) ((  void (*) (UnityEvent_1_t114412532 *, UnityAction_1_t1442648268 *, const MethodInfo*))UnityEvent_1_AddListener_m22503421_gshared)(__this, ___call0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_RemoveListener_m726453490(__this, ___call0, method) ((  void (*) (UnityEvent_1_t114412532 *, UnityAction_1_t1442648268 *, const MethodInfo*))UnityEvent_1_RemoveListener_m4278264272_gshared)(__this, ___call0, method)
// System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::FindMethod_Impl(System.String,System.Object)
#define UnityEvent_1_FindMethod_Impl_m3341265297(__this, ___name0, ___targetObj1, method) ((  MethodInfo_t * (*) (UnityEvent_1_t114412532 *, String_t*, Il2CppObject *, const MethodInfo*))UnityEvent_1_FindMethod_Impl_m2223850067_gshared)(__this, ___name0, ___targetObj1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::GetDelegate(System.Object,System.Reflection.MethodInfo)
#define UnityEvent_1_GetDelegate_m447164749(__this, ___target0, ___theFunction1, method) ((  BaseInvokableCall_t2229564840 * (*) (UnityEvent_1_t114412532 *, Il2CppObject *, MethodInfo_t *, const MethodInfo*))UnityEvent_1_GetDelegate_m669290055_gshared)(__this, ___target0, ___theFunction1, method)
// UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
#define UnityEvent_1_GetDelegate_m1126055514(__this /* static, unused */, ___action0, method) ((  BaseInvokableCall_t2229564840 * (*) (Il2CppObject * /* static, unused */, UnityAction_1_t1442648268 *, const MethodInfo*))UnityEvent_1_GetDelegate_m3098147632_gshared)(__this /* static, unused */, ___action0, method)
// System.Void UnityEngine.Events.UnityEvent`1<Lean.Touch.LeanFinger>::Invoke(T0)
#define UnityEvent_1_Invoke_m3195227978(__this, ___arg00, method) ((  void (*) (UnityEvent_1_t114412532 *, LeanFinger_t76062517 *, const MethodInfo*))UnityEvent_1_Invoke_m838874366_gshared)(__this, ___arg00, method)
