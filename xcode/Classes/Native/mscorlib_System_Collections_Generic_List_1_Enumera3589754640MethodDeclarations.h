﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanDragTrail/Link>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3991753251(__this, ___l0, method) ((  void (*) (Enumerator_t3589754640 *, List_1_t4055024966 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanDragTrail/Link>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4232969463(__this, method) ((  void (*) (Enumerator_t3589754640 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanDragTrail/Link>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1265193675(__this, method) ((  Il2CppObject * (*) (Enumerator_t3589754640 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanDragTrail/Link>::Dispose()
#define Enumerator_Dispose_m388478182(__this, method) ((  void (*) (Enumerator_t3589754640 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanDragTrail/Link>::VerifyState()
#define Enumerator_VerifyState_m1808372669(__this, method) ((  void (*) (Enumerator_t3589754640 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanDragTrail/Link>::MoveNext()
#define Enumerator_MoveNext_m3835292723(__this, method) ((  bool (*) (Enumerator_t3589754640 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanDragTrail/Link>::get_Current()
#define Enumerator_get_Current_m1163782544(__this, method) ((  Link_t390936538 * (*) (Enumerator_t3589754640 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
