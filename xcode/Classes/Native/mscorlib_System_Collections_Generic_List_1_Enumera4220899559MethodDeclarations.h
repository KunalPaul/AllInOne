﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanTouch>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3141714947(__this, ___l0, method) ((  void (*) (Enumerator_t4220899559 *, List_1_t391202589 *, const MethodInfo*))Enumerator__ctor_m3769601633_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanTouch>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2223334599(__this, method) ((  void (*) (Enumerator_t4220899559 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3440386353_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanTouch>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3023532591(__this, method) ((  Il2CppObject * (*) (Enumerator_t4220899559 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2853089017_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanTouch>::Dispose()
#define Enumerator_Dispose_m562125644(__this, method) ((  void (*) (Enumerator_t4220899559 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanTouch>::VerifyState()
#define Enumerator_VerifyState_m1279616453(__this, method) ((  void (*) (Enumerator_t4220899559 *, const MethodInfo*))Enumerator_VerifyState_m825848279_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanTouch>::MoveNext()
#define Enumerator_MoveNext_m727003971(__this, method) ((  bool (*) (Enumerator_t4220899559 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Lean.Touch.LeanTouch>::get_Current()
#define Enumerator_get_Current_m1309036504(__this, method) ((  LeanTouch_t1022081457 * (*) (Enumerator_t4220899559 *, const MethodInfo*))Enumerator_get_Current_m3108634708_gshared)(__this, method)
